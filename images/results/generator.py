import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('pdf')

import numpy as np

from groundTruth import baryonDensity4x4, baryonDensity2x2, energyDensity4x4, energyDensity2x2, baryonDensity2x4, energyDensity2x4, baryonDensity4x2, energyDensity4x2

dpi = 600
truthWidth = 0.5
outfolder = "out/"

deviationcounter = 0
totalcounter = 0

def readFile(filename):
    points = {}
    with open(filename) as file:
        lines = [line.replace(" ", "").split(",") for line in file.read().split("\n")]
        labels = lines.pop(0)
        lines.pop(-1)
        for index, label in enumerate(labels):
            values = []
            for line in lines:
                values.append(float(line[index]))
            points[label] = values
    return points


for filename, groundTruthBaryons, groundTruthEnergy, saveto in [("2x2x1x1_thesis_final", baryonDensity2x2, energyDensity2x2, "2x2"),
                                                                ("2x4x1x1_thesis_final", baryonDensity2x4, energyDensity2x4, "2x4"),
                                                                ("4x2x1x1_thesis_final", baryonDensity4x2, energyDensity4x2, "4x2"),
                                                                ("4x4x1x1_thesis_final", baryonDensity4x4, energyDensity4x4, "4x4"),
                                                                ]:
    for gamma in [0.5, 1, 1.5]:
        file = readFile(f"data/{filename}_{gamma}.csv")
        print(f"plotting {filename}, gamma={gamma}")
        current_values = dict()
        for key in file:
            current_values[key] = [x for index, x in enumerate(file[key]) if file["gamma"][index] == gamma and file["mu"][index] >= 0]

        fig, ax = plt.subplots()

        fig.set_figwidth(6.6)
        fig.set_figheight(6)
        ax.set_xlabel(r"\mu")
        ax.set_ylabel(r"$\rho_B$")

        ax.errorbar(current_values["mu"], current_values["baryonDensityAverage"], current_values["baryonDensityError"], ecolor="r", marker=".", label="worm", linestyle="none", linewidth=0.5, barsabove=True)
        ax.plot(np.arange(min(current_values["mu"]), max(current_values["mu"]) + 0.01, 0.01), [groundTruthBaryons(x1, 0, gamma) for x1 in np.arange(min(current_values["mu"]), max(current_values["mu"]) + 0.01, 0.01)], label=r"analytic", linewidth=truthWidth)


        ax.grid()
        ax.legend()

        plt.title(fr"$\gamma={gamma}$")

        plt.savefig(f"{outfolder}{saveto}_baryonD_g={gamma}.pdf", dpi=dpi)

        plt.close(fig)


        #######################################################################################################################################fig, ax = plt.subplots()

        fig, ax = plt.subplots()

        fig.set_figwidth(6.6)
        fig.set_figheight(6)
        ax.set_xlabel(r"\mu")
        ax.set_ylabel(r"$\tilde{\epsilon}$")

        ax.errorbar(current_values["mu"], current_values["energyDensityAverage"], current_values["energyDensityError"], ecolor="r", marker=".", label="worm", linestyle="none", linewidth=0.5, barsabove=True)
        ax.plot(np.arange(min(current_values["mu"]), max(current_values["mu"]) + 0.01, 0.01), [groundTruthEnergy(x1, 0, gamma) for x1 in np.arange(min(current_values["mu"]), max(current_values["mu"]) + 0.01, 0.01)], label="analytic", linewidth=truthWidth)


        ax.grid()
        ax.legend()

        plt.title(fr"$\gamma={gamma}$")

        plt.savefig(f"{outfolder}{saveto}_energyD_g={gamma}.pdf", dpi=dpi)

        plt.close(fig)

        #######################################################################################################################################


        fig, ax = plt.subplots()

        fig.set_figwidth(6.6)
        fig.set_figheight(6)
        ax.set_xlabel(r"\mu")
        ax.set_ylabel(r"$\tilde{\epsilon}$")

        values = [value - truth for value, truth in zip(current_values["energyDensityAverage"], [groundTruthEnergy(x1, 0, gamma) for x1 in current_values["mu"]])]

        ax.errorbar(current_values["mu"], values, current_values["energyDensityError"], ecolor="r", marker=".", label="worm-analytic", linestyle="none", linewidth=0.5, barsabove=True)

        ax.grid()
        ax.legend()

        plt.title(fr"$\gamma={gamma}$")

        plt.savefig(f"{outfolder}{saveto}_energyDdiff_g={gamma}.pdf", dpi=dpi)

        plt.close(fig)

        for value, error in zip(values, current_values["energyDensityError"]):
            totalcounter += 1
            if abs(value) > error:
                deviationcounter += 1

        #######################################################################################################################################


        fig, ax = plt.subplots()

        fig.set_figwidth(6.6)
        fig.set_figheight(6)
        ax.set_xlabel(r"\mu")
        ax.set_ylabel(r"$\rho_B$")

        values = [value - truth for value, truth in zip(current_values["baryonDensityAverage"], [groundTruthBaryons(x1, 0, gamma) for x1 in current_values["mu"]])]

        ax.errorbar(current_values["mu"], values, current_values["baryonDensityError"], ecolor="r", marker=".", label=r"worm-analytic", linestyle="none", linewidth=0.5, barsabove=True)

        ax.grid()
        ax.legend()

        plt.title(fr"$\gamma={gamma}$")

        plt.savefig(f"{outfolder}{saveto}_baryonDdiff_g={gamma}.pdf", dpi=dpi)

        plt.close(fig)

        for value, error in zip(values, current_values["baryonDensityError"]):
            totalcounter += 1
            if abs(value) > error:
                deviationcounter += 1

        #######################################################################################################################################

        fig, ax = plt.subplots()

        ax.set_ylim(0,1.1)

        fig.set_figwidth(6.6)
        fig.set_figheight(6)
        ax.set_xlabel(r"\mu")
        #ax.errorbar(current_values["mu"], current_values["baryonDensityAverage"], current_values["baryonDensityError"], ecolor="r", marker=".", label=r"$\langle\rho_B\rangle$", linestyle="none", linewidth=0.5, barsabove=True)

        ax.errorbar(current_values["mu"], current_values["signAverage"], current_values["signError"], ecolor="r", marker=".", label=r"$\langle\sigma\rangle$", linestyle="none", linewidth=0.5, barsabove=True)

        ax.grid()
        ax.legend()

        plt.title(fr"$\gamma={gamma}$")

        plt.savefig(outfolder+"sign_"+saveto+f"_g={gamma}.pdf", dpi=dpi)

        plt.close(fig)

print(f"percentage of datapoints outside of one standard deviation: {float(deviationcounter) / totalcounter}, percentage inside one standard deviation: {1-float(deviationcounter) / totalcounter}, total outside: {deviationcounter}, total points: {totalcounter}")


for filename, saveto in [("4x4x4x4", "4x4x4x4"),
                         ("2x2x2x2", "2x2x2x2"),
                         ("2x2x2x1", "2x2x2"),
                         ("4x4x4x1", "4x4x4"),
                         ("8x8x1x1", "8x8"),
                         ("16x16x1x1", "16x16"),
                         ]:
        for gamma in [0.5, 1, 1.5]:
            file = readFile(f"data/{filename}_thesis_final_{gamma}.csv")
            print(f"plotting {filename}, gamma={gamma}")
            current_values = dict()
            for key in file:
                current_values[key] = [x for index, x in enumerate(file[key]) if file["gamma"][index] == gamma and file["mu"][index] >= 0]

            fig, ax = plt.subplots()

            fig.set_figwidth(6.6)
            fig.set_figheight(6)
            ax.set_xlabel(r"\mu")
            ax.set_ylabel(r"$\tilde{\epsilon}$")

            ax.errorbar(current_values["mu"], current_values["energyDensityAverage"], current_values["energyDensityError"], ecolor="r", marker=".", label="worm", linestyle="none", linewidth=0.5, barsabove=True)

            ax.grid()
            ax.legend()

            plt.title(fr"$\gamma={gamma}$")

            plt.savefig(f"{outfolder}{saveto}_energyD_g={gamma}.pdf", dpi=dpi)

            plt.close(fig)

            ####################################################################################################################


            fig, ax = plt.subplots()

            fig.set_figwidth(6.6)
            fig.set_figheight(6)
            ax.set_xlabel(r"\mu")
            ax.set_ylabel(r"$\rho_B$")

            ax.errorbar(current_values["mu"], current_values["baryonDensityAverage"], current_values["baryonDensityError"], ecolor="r", marker=".", label=r"worm", linestyle="none", linewidth=0.5, barsabove=True)

            ax.grid()
            ax.legend()

            plt.title(fr"$\gamma={gamma}$")

            plt.savefig(f"{outfolder}{saveto}_baryonD_g={gamma}.pdf", dpi=dpi)

            plt.close(fig)


            ####################################################################################################################

            fig, ax = plt.subplots()

            fig.set_figwidth(6.6)
            fig.set_figheight(6)
            ax.set_xlabel(r"\mu")

            ax.errorbar(current_values["mu"], current_values["signAverage"], current_values["signError"], ecolor="r", marker=".", label=r"$\langle\sigma\rangle$", linestyle="none", linewidth=0.5, barsabove=True)

            ax.grid()
            ax.legend()

            plt.title(fr"$\gamma={gamma}$")

            plt.savefig(outfolder+"sign_"+saveto+f"_g={gamma}.pdf", dpi=dpi)

            plt.close(fig)
