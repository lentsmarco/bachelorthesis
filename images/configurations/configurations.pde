import processing.pdf.*;

final int d = 2;



final PVector borders = new PVector(150, 150);
final float siteSize = 15;
final float mesonLineSpacing = 5;
final float baryonArrowSize = 5;
PVector spacing;

final color mesonColor = color(255,0,0);
final color baryonColor = color(0,157,255);
final color WormHeadColor = color(0,255,0);

latticePoint[][] lattice;
int N_t;
int N_x;

class latticePoint{
  int t;
  int x;
  int n;
  int m;
  boolean head;
  int[][] k = new int[d][2];
  int[][] b = new int[d][2];
  latticePoint(int[] args){
    t = args[0];
    x = args[1];
    n = args[2];
    m = args[3];
    for(int mu = 0; mu < d; mu++)
    for(int forward = 0; forward < 2; forward++){
      k[mu][forward] = args[4 + (mu * 2 + forward) * 2];
      b[mu][forward] = args[5 + (mu * 2 + forward) * 2];
    }
  }

  void show(){
    float[] pixelCoords = convertToPixels(x, t);
    fill(255);
    if(head) fill(WormHeadColor);
    if(n == 1) fill(mesonColor);
    if(m == 1) fill(baryonColor);
    ellipse(pixelCoords[0], pixelCoords[1], siteSize, siteSize);
    if(head && (n == 1 || m == 1)){
      fill(WormHeadColor);
      arc(pixelCoords[0], pixelCoords[1], siteSize, siteSize, -HALF_PI, HALF_PI);
    }
    for(int mu = 0; mu < d; mu++)
    for(int forward = 0; forward < 2; forward++){
      drawMesonLine(pixelCoords[0], pixelCoords[1], mu, forward, k[mu][forward]);
      drawBaryonLine(pixelCoords[0], pixelCoords[1], mu, forward, b[mu][forward]);
    }
  }
}

void setup(){
  size(700, 700);
}

void draw(){ 
  // starting configuration
  N_t = 4;
  N_x = 4;
  spacing = new PVector((width - 2 * borders.x) / (N_x - 1), (height - 2 * borders.y) / (N_t - 1));
  lattice = new latticePoint[N_t][N_x];
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    if(t % 2 == 0){
      int[] args = {t, x, 0, 0, 1, 0, 2, 0, 0, 0, 0, 0};
      lattice[t][x] = new latticePoint(args);
    }else{
      int[] args = {t, x, 0, 0, 2, 0, 1, 0, 0, 0, 0, 0};
      lattice[t][x] = new latticePoint(args);
    }
  }
  beginRecord(PDF, "out/starting_conf.pdf");
  background(255);
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    lattice[t][x].show();
  }
  endRecord();
  
  // possible links
  N_t = 2;
  N_x = 4;
  spacing = new PVector((width - 2 * borders.x) / (N_x - 1), (height - 2 * borders.y) / (N_t - 1));
  lattice = new latticePoint[N_t][N_x];
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    if(x != 3){
      if(t % 2 == 0){
        int[] args = {t, x, 0, 0, 0, 0, x+1, 0, 0, 0, 0, 0};
        lattice[t][x] = new latticePoint(args);
      }else{
        int[] args = {t, x, 0, 0, x+1, 0, 0, 0, 0, 0, 0, 0};
        lattice[t][x] = new latticePoint(args);
      }
    }else{
      if(t % 2 == 0){
        int[] args = {t, x, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0};
        lattice[t][x] = new latticePoint(args);
      }else{
        int[] args = {t, x, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0};
        lattice[t][x] = new latticePoint(args);
      }
    }
  }
  beginRecord(PDF, "out/links.pdf");
  background(255);
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    lattice[t][x].show();
  }
  endRecord();
  
  // meson move 1_1
  N_t = 3;
  N_x = 3;
  spacing = new PVector((width - 2 * borders.x) / (N_x - 1), (height - 2 * borders.y) / (N_t - 1));
  lattice = new latticePoint[N_t][N_x];
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    if(t % 2 == 0){
      int[] args = {t, x, 0, 0, 1, 0, 2, 0, 0, 0, 0, 0};
      lattice[t][x] = new latticePoint(args);
    }else{
      int[] args = {t, x, 0, 0, 2, 0, 1, 0, 0, 0, 0, 0};
      lattice[t][x] = new latticePoint(args);
    }
  }
  lattice[1][1].k[1][1]=1;
  lattice[1][1].k[0][0]=1;
  lattice[0][1].k[0][1]=1;
  lattice[1][2].k[1][0]=1;
  lattice[1][2].k[0][1]=0;
  lattice[2][2].k[0][0]=0;
  
  beginRecord(PDF, "out/meson_step1_1.pdf");
  background(255);
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    lattice[t][x].show();
  }
  endRecord();
  
  // meson move 1_2
  N_t = 3;
  N_x = 3;
  spacing = new PVector((width - 2 * borders.x) / (N_x - 1), (height - 2 * borders.y) / (N_t - 1));
  lattice = new latticePoint[N_t][N_x];
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    if(t % 2 == 0){
      int[] args = {t, x, 0, 0, 1, 0, 2, 0, 0, 0, 0, 0};
      lattice[t][x] = new latticePoint(args);
    }else{
      int[] args = {t, x, 0, 0, 2, 0, 1, 0, 0, 0, 0, 0};
      lattice[t][x] = new latticePoint(args);
    }
  }
  lattice[1][1].k[1][1]=0;
  lattice[1][1].n=1;
  lattice[1][1].k[0][0]=1;
  lattice[0][1].k[0][1]=1;
  lattice[1][2].k[1][0]=1;
  lattice[1][2].k[0][1]=0;
  lattice[1][2].head = true;
  lattice[2][2].k[0][0]=0;
  
  beginRecord(PDF, "out/meson_step1_2.pdf");
  background(255);
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    lattice[t][x].show();
  }
  endRecord();
  
  // meson move 2_1
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    int[] args = {t, x, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    lattice[t][x] = new latticePoint(args);
  }
  
  lattice[0][0].k[0][0] = 0;
  lattice[0][0].k[0][1] = 1;
  lattice[0][0].k[1][0] = 0;
  lattice[0][0].k[1][1] = 2;
  
  lattice[0][1].k[0][0] = 1;
  lattice[0][1].k[0][1] = 0;
  lattice[0][1].k[1][0] = 2;
  lattice[0][1].k[1][1] = 0;
  
  lattice[0][2].k[0][0] = 2;
  lattice[0][2].k[0][1] = 1;
  lattice[0][2].k[1][0] = 0;
  lattice[0][2].k[1][1] = 0;
  
  lattice[1][0].k[0][0] = 1;
  lattice[1][0].k[0][1] = 2;
  lattice[1][0].k[1][0] = 0;
  lattice[1][0].k[1][1] = 0;
  
  lattice[1][1].k[0][0] = 1;
  lattice[1][1].k[0][1] = 1;
  lattice[1][1].k[1][0] = 0;
  lattice[1][1].k[1][1] = 1;
  lattice[1][1].head = true;
  
  
  lattice[1][2].k[0][0] = 1;
  lattice[1][2].k[0][1] = 0;
  lattice[1][2].k[1][0] = 1;
  lattice[1][2].k[1][1] = 1;
  
  lattice[2][0].k[0][0] = 2;
  lattice[2][0].k[0][1] = 0;
  lattice[2][0].k[1][0] = 0;
  lattice[2][0].k[1][1] = 1;
  
  lattice[2][1].k[0][0] = 1;
  lattice[2][1].k[0][1] = 1;
  lattice[2][1].k[1][0] = 1;
  lattice[2][1].k[1][1] = 0;
  
  lattice[2][2].k[0][0] = 0;
  lattice[2][2].k[0][1] = 0;
  lattice[2][2].k[1][0] = 0;
  lattice[2][2].k[1][1] = 3;
  
  beginRecord(PDF, "out/meson_step2_1.pdf");
  background(255);
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    lattice[t][x].show();
  }
  endRecord();
  
  // meson move 2_2
  
  lattice[0][0].k[0][0] = 0;
  lattice[0][0].k[0][1] = 1;
  lattice[0][0].k[1][0] = 0;
  lattice[0][0].k[1][1] = 2;
  
  lattice[0][1].k[0][0] = 1;
  lattice[0][1].k[0][1] = 0;
  lattice[0][1].k[1][0] = 2;
  lattice[0][1].k[1][1] = 0;
  
  lattice[0][2].k[0][0] = 2;
  lattice[0][2].k[0][1] = 1;
  lattice[0][2].k[1][0] = 0;
  lattice[0][2].k[1][1] = 0;
  
  lattice[1][0].k[0][0] = 1;
  lattice[1][0].k[0][1] = 2;
  lattice[1][0].k[1][0] = 0;
  lattice[1][0].k[1][1] = 0;
  
  lattice[1][1].k[0][0] = 0;
  lattice[1][1].k[0][1] = 2;
  lattice[1][1].k[1][0] = 0;
  lattice[1][1].k[1][1] = 1;
  lattice[1][1].head = false;
  
  lattice[1][2].k[0][0] = 1;
  lattice[1][2].k[0][1] = 0;
  lattice[1][2].k[1][0] = 1;
  lattice[1][2].k[1][1] = 1;
  
  lattice[2][0].k[0][0] = 2;
  lattice[2][0].k[0][1] = 0;
  lattice[2][0].k[1][0] = 0;
  lattice[2][0].k[1][1] = 1;
  
  lattice[2][1].k[0][0] = 1;
  lattice[2][1].k[0][1] = 1;
  lattice[2][1].k[1][0] = 1;
  lattice[2][1].k[1][1] = 0;
  lattice[2][1].head = true;
  
  lattice[2][2].k[0][0] = 0;
  lattice[2][2].k[0][1] = 0;
  lattice[2][2].k[1][0] = 0;
  lattice[2][2].k[1][1] = 3;
  
  beginRecord(PDF, "out/meson_step2_2.pdf");
  background(255);
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    lattice[t][x].show();
  }
  endRecord();
  
  // meson move 3_1
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    int[] args = {t, x, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    lattice[t][x] = new latticePoint(args);
  }
  
  lattice[0][0].k[0][0] = 1;
  lattice[0][0].k[0][1] = 1;
  lattice[0][0].k[1][0] = 1;
  lattice[0][0].k[1][1] = 0;
  
  lattice[0][1].k[0][0] = 0;
  lattice[0][1].k[0][1] = 1;
  lattice[0][1].k[1][0] = 0;
  lattice[0][1].k[1][1] = 2;
  
  lattice[0][2].k[0][0] = 1;
  lattice[0][2].k[0][1] = 0;
  lattice[0][2].k[1][0] = 2;
  lattice[0][2].k[1][1] = 0;
  
  lattice[1][0].k[0][0] = 1;
  lattice[1][0].k[0][1] = 0;
  lattice[1][0].k[1][0] = 1;
  lattice[1][0].k[1][1] = 1;
  
  lattice[1][1].k[0][0] = 0;
  lattice[1][1].k[0][1] = 1;
  lattice[1][1].k[1][0] = 1;
  lattice[1][1].k[1][1] = 0;
  lattice[1][1].n = 1;
  lattice[1][1].head = true;
  
  
  lattice[1][2].k[0][0] = 0;
  lattice[1][2].k[0][1] = 3;
  lattice[1][2].k[1][0] = 0;
  lattice[1][2].k[1][1] = 0;
  
  lattice[2][0].k[0][0] = 0;
  lattice[2][0].k[0][1] = 1;
  lattice[2][0].k[1][0] = 0;
  lattice[2][0].k[1][1] = 2;
  
  lattice[2][1].k[0][0] = 1;
  lattice[2][1].k[0][1] = 0;
  lattice[2][1].k[1][0] = 2;
  lattice[2][1].k[1][1] = 0;
  
  lattice[2][2].k[0][0] = 3;
  lattice[2][2].k[0][1] = 0;
  lattice[2][2].k[1][0] = 0;
  lattice[2][2].k[1][1] = 0;
  
  beginRecord(PDF, "out/meson_step3_1.pdf");
  background(255);
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    lattice[t][x].show();
  }
  endRecord();
  
  // meson move 3_2
  
  lattice[0][0].k[0][0] = 1;
  lattice[0][0].k[0][1] = 1;
  lattice[0][0].k[1][0] = 1;
  lattice[0][0].k[1][1] = 0;
  
  lattice[0][1].k[0][0] = 0;
  lattice[0][1].k[0][1] = 1;
  lattice[0][1].k[1][0] = 0;
  lattice[0][1].k[1][1] = 2;
  
  lattice[0][2].k[0][0] = 1;
  lattice[0][2].k[0][1] = 0;
  lattice[0][2].k[1][0] = 2;
  lattice[0][2].k[1][1] = 0;
  
  lattice[1][0].k[0][0] = 1;
  lattice[1][0].k[0][1] = 0;
  lattice[1][0].k[1][0] = 1;
  lattice[1][0].k[1][1] = 1;
  
  lattice[1][1].k[0][0] = 1;
  lattice[1][1].k[0][1] = 1;
  lattice[1][1].k[1][0] = 1;
  lattice[1][1].k[1][1] = 0;
  lattice[1][1].n = 0;
  lattice[1][1].head = false;
  
  
  lattice[1][2].k[0][0] = 0;
  lattice[1][2].k[0][1] = 3;
  lattice[1][2].k[1][0] = 0;
  lattice[1][2].k[1][1] = 0;
  
  lattice[2][0].k[0][0] = 0;
  lattice[2][0].k[0][1] = 1;
  lattice[2][0].k[1][0] = 0;
  lattice[2][0].k[1][1] = 2;
  
  lattice[2][1].k[0][0] = 1;
  lattice[2][1].k[0][1] = 0;
  lattice[2][1].k[1][0] = 2;
  lattice[2][1].k[1][1] = 0;
  
  lattice[2][2].k[0][0] = 3;
  lattice[2][2].k[0][1] = 0;
  lattice[2][2].k[1][0] = 0;
  lattice[2][2].k[1][1] = 0;
  
  beginRecord(PDF, "out/meson_step3_2.pdf");
  background(255);
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    lattice[t][x].show();
  }
  endRecord();
  
  // meson move 3_3
  
  lattice[0][0].k[0][0] = 1;
  lattice[0][0].k[0][1] = 1;
  lattice[0][0].k[1][0] = 1;
  lattice[0][0].k[1][1] = 0;
  
  lattice[0][1].k[0][0] = 0;
  lattice[0][1].k[0][1] = 1;
  lattice[0][1].k[1][0] = 0;
  lattice[0][1].k[1][1] = 2;
  
  lattice[0][2].k[0][0] = 1;
  lattice[0][2].k[0][1] = 0;
  lattice[0][2].k[1][0] = 2;
  lattice[0][2].k[1][1] = 0;
  
  lattice[1][0].k[0][0] = 1;
  lattice[1][0].k[0][1] = 0;
  lattice[1][0].k[1][0] = 1;
  lattice[1][0].k[1][1] = 1;
  lattice[1][0].head = true;
  
  lattice[1][1].k[0][0] = 1;
  lattice[1][1].k[0][1] = 1;
  lattice[1][1].k[1][0] = 0;
  lattice[1][1].k[1][1] = 0;
  lattice[1][1].n = 1;
  lattice[1][1].head = false;
  
  
  lattice[1][2].k[0][0] = 0;
  lattice[1][2].k[0][1] = 3;
  lattice[1][2].k[1][0] = 0;
  lattice[1][2].k[1][1] = 0;
  
  lattice[2][0].k[0][0] = 0;
  lattice[2][0].k[0][1] = 1;
  lattice[2][0].k[1][0] = 0;
  lattice[2][0].k[1][1] = 2;
  
  lattice[2][1].k[0][0] = 1;
  lattice[2][1].k[0][1] = 0;
  lattice[2][1].k[1][0] = 2;
  lattice[2][1].k[1][1] = 0;
  
  lattice[2][2].k[0][0] = 3;
  lattice[2][2].k[0][1] = 0;
  lattice[2][2].k[1][0] = 0;
  lattice[2][2].k[1][1] = 0;
  
  beginRecord(PDF, "out/meson_step3_3.pdf");
  background(255);
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    lattice[t][x].show();
  }
  endRecord();
  
  // baryon move 1_1
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    int[] args = {t, x, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    lattice[t][x] = new latticePoint(args);
  }
  
  lattice[0][0].k[0][0] = 2;
  lattice[0][0].k[0][1] = 0;
  lattice[0][0].k[1][0] = 1;
  lattice[0][0].k[1][1] = 0;
  
  lattice[0][1].b[0][0] = 1;
  lattice[0][1].b[0][1] = -1;
  lattice[0][1].b[1][0] = 0;
  lattice[0][1].b[1][1] = 0;
  
  lattice[0][2].k[0][0] = 1;
  lattice[0][2].k[0][1] = 0;
  lattice[0][2].k[1][0] = 0;
  lattice[0][2].k[1][1] = 2;
 
  lattice[1][0].k[0][0] = 0;
  lattice[1][0].k[0][1] = 3;
  lattice[1][0].k[1][0] = 0;
  lattice[1][0].k[1][1] = 0;
  
  lattice[1][1].b[0][0] = 1;
  lattice[1][1].b[0][1] = 0;
  lattice[1][1].b[1][0] = 0;
  lattice[1][1].b[1][1] = -1;
  
  
  lattice[1][2].b[0][0] = 0;
  lattice[1][2].b[0][1] = 0;
  lattice[1][2].b[1][0] = 1;
  lattice[1][2].b[1][1] = -1;
  
  lattice[2][0].k[0][0] = 3;
  lattice[2][0].k[0][1] = 0;
  lattice[2][0].k[1][0] = 0;
  lattice[2][0].k[1][1] = 0;
  
  lattice[2][1].b[0][0] = 0;
  lattice[2][1].b[0][1] = -1;
  lattice[2][1].b[1][0] = 0;
  lattice[2][1].b[1][1] = 1;
  
  lattice[2][2].b[0][0] = 0;
  lattice[2][2].b[0][1] = 0;
  lattice[2][2].b[1][0] = -1;
  lattice[2][2].b[1][1] = 1;
  
  beginRecord(PDF, "out/baryon_step1_1.pdf");
  background(255);
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    lattice[t][x].show();
  }
  endRecord();
  
  // baryon move 1_2
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    int[] args = {t, x, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    lattice[t][x] = new latticePoint(args);
  }
  
  lattice[0][0].k[0][0] = 2;
  lattice[0][0].k[0][1] = 0;
  lattice[0][0].k[1][0] = 1;
  lattice[0][0].k[1][1] = 0;
  
  lattice[0][1].b[0][0] = 1;
  lattice[0][1].b[0][1] = -1;
  lattice[0][1].b[1][0] = 0;
  lattice[0][1].b[1][1] = 0;
  
  lattice[0][2].k[0][0] = 1;
  lattice[0][2].k[0][1] = 0;
  lattice[0][2].k[1][0] = 0;
  lattice[0][2].k[1][1] = 2;
 
  lattice[1][0].k[0][0] = 0;
  lattice[1][0].k[0][1] = 3;
  lattice[1][0].k[1][0] = 0;
  lattice[1][0].k[1][1] = 0;
  
  lattice[1][1].b[0][0] = 1;
  lattice[1][1].b[0][1] = 0;
  lattice[1][1].b[1][0] = 0;
  lattice[1][1].b[1][1] = 0;
  lattice[1][1].m = 1;
  
  
  
  lattice[1][2].b[0][0] = 0;
  lattice[1][2].b[0][1] = 0;
  lattice[1][2].b[1][0] = 1;
  lattice[1][2].b[1][1] = -1;
  lattice[1][2].head = true;
  
  lattice[2][0].k[0][0] = 3;
  lattice[2][0].k[0][1] = 0;
  lattice[2][0].k[1][0] = 0;
  lattice[2][0].k[1][1] = 0;
  
  lattice[2][1].b[0][0] = 0;
  lattice[2][1].b[0][1] = -1;
  lattice[2][1].b[1][0] = 0;
  lattice[2][1].b[1][1] = 1;
  
  lattice[2][2].b[0][0] = 0;
  lattice[2][2].b[0][1] = 0;
  lattice[2][2].b[1][0] = -1;
  lattice[2][2].b[1][1] = 1;
  
  beginRecord(PDF, "out/baryon_step1_2.pdf");
  background(255);
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    lattice[t][x].show();
  }
  endRecord();
  
  // baryon move 3_3
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    int[] args = {t, x, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    lattice[t][x] = new latticePoint(args);
  }
  
  lattice[0][0].b[0][0] = 0;
  lattice[0][0].b[0][1] = -1;
  lattice[0][0].b[1][0] = 0;
  lattice[0][0].b[1][1] = 1;
  
  lattice[0][1].b[0][0] = 1;
  lattice[0][1].b[0][1] = 0;
  lattice[0][1].b[1][0] = -1;
  lattice[0][1].b[1][1] = 0;
  
  lattice[0][2].k[0][0] = 2;
  lattice[0][2].k[0][1] = 0;
  lattice[0][2].k[1][0] = 0;
  lattice[0][2].k[1][1] = 1;
  
  lattice[1][0].b[0][0] = 1;
  lattice[1][0].b[0][1] = -1;
  lattice[1][0].b[1][0] = 0;
  lattice[1][0].b[1][1] = 0;
  
  lattice[1][1].k[0][0] = 0;
  lattice[1][1].k[0][1] = 0;
  lattice[1][1].k[1][0] = 0;
  lattice[1][1].k[1][1] = 3;
  lattice[1][1].m = 0;
  
  lattice[1][2].k[0][0] = 0;
  lattice[1][2].k[0][1] = 0;
  lattice[1][2].k[1][0] = 3;
  lattice[1][2].k[1][1] = 0;
  
  lattice[2][0].b[0][0] = 1;
  lattice[2][0].b[0][1] = -1;
  lattice[2][0].b[1][0] = 0;
  lattice[2][0].b[1][1] = 0;
  
  lattice[2][1].k[0][0] = 0;
  lattice[2][1].k[0][1] = 2;
  lattice[2][1].k[1][0] = 0;
  lattice[2][1].k[1][1] = 1;
  
  lattice[2][2].k[0][0] = 0;
  lattice[2][2].k[0][1] = 1;
  lattice[2][2].k[1][0] = 1;
  lattice[2][2].k[1][1] = 1;
  
  beginRecord(PDF, "out/baryon_step1_3.pdf");
  background(255);
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    lattice[t][x].show();
  }
  endRecord();
  
  // baryon move 3_4
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    int[] args = {t, x, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    lattice[t][x] = new latticePoint(args);
  }
  
  lattice[0][0].b[0][0] = 0;
  lattice[0][0].b[0][1] = -1;
  lattice[0][0].b[1][0] = 0;
  lattice[0][0].b[1][1] = 1;
  
  lattice[0][1].b[0][0] = 1;
  lattice[0][1].b[0][1] = 0;
  lattice[0][1].b[1][0] = -1;
  lattice[0][1].b[1][1] = 0;
  
  lattice[0][2].k[0][0] = 2;
  lattice[0][2].k[0][1] = 0;
  lattice[0][2].k[1][0] = 0;
  lattice[0][2].k[1][1] = 1;
  
  lattice[1][0].b[0][0] = 1;
  lattice[1][0].b[0][1] = -1;
  lattice[1][0].b[1][0] = 0;
  lattice[1][0].b[1][1] = 0;
  
  lattice[1][1].b[0][0] = 0;
  lattice[1][1].b[0][1] = 0;
  lattice[1][1].b[1][0] = 0;
  lattice[1][1].b[1][1] = 1;
  lattice[1][1].m = 1;
  
  lattice[1][2].k[0][0] = 0;
  lattice[1][2].k[0][1] = 0;
  lattice[1][2].k[1][0] = 3;
  lattice[1][2].k[1][1] = 0;
  lattice[1][2].head = true;
  
  lattice[2][0].b[0][0] = 1;
  lattice[2][0].b[0][1] = -1;
  lattice[2][0].b[1][0] = 0;
  lattice[2][0].b[1][1] = 0;
  
  lattice[2][1].k[0][0] = 0;
  lattice[2][1].k[0][1] = 2;
  lattice[2][1].k[1][0] = 0;
  lattice[2][1].k[1][1] = 1;
  
  lattice[2][2].k[0][0] = 0;
  lattice[2][2].k[0][1] = 1;
  lattice[2][2].k[1][0] = 1;
  lattice[2][2].k[1][1] = 1;
  
  beginRecord(PDF, "out/baryon_step1_4.pdf");
  background(255);
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    lattice[t][x].show();
  }
  endRecord();
  
  // baryon move 2_1
  
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    int[] args = {t, x, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    lattice[t][x] = new latticePoint(args);
  }
  
  lattice[0][0].b[0][0] = 0;
  lattice[0][0].b[0][1] = 1;
  lattice[0][0].b[1][0] = -1;
  lattice[0][0].b[1][1] = 0;
  
  lattice[0][1].b[0][0] = -1;
  lattice[0][1].b[0][1] = 1;
  lattice[0][1].b[1][0] = 0;
  lattice[0][1].b[1][1] = 0;
  
  lattice[0][2].b[0][0] = 1;
  lattice[0][2].b[0][1] = -1;
  lattice[0][2].b[1][0] = 0;
  lattice[0][2].b[1][1] = 0;
  
  lattice[1][0].b[0][0] = -1;
  lattice[1][0].b[0][1] = 1;
  lattice[1][0].b[1][0] = 0;
  lattice[1][0].b[1][1] = 0;
  
  lattice[1][1].k[0][0] = 3;
  lattice[1][1].k[0][1] = 0;
  lattice[1][1].k[1][0] = 0;
  lattice[1][1].k[1][1] = 0;
  lattice[1][1].head = true;
  
  lattice[1][2].b[0][0] = 1;
  lattice[1][2].b[0][1] = 0;
  lattice[1][2].b[1][0] = 0;
  lattice[1][2].b[1][1] = -1;
  
  lattice[2][0].b[0][0] = -1;
  lattice[2][0].b[0][1] = 0;
  lattice[2][0].b[1][0] = 0;
  lattice[2][0].b[1][1] = 1;
  
  lattice[2][1].b[0][0] = 0;
  lattice[2][1].b[0][1] = 1;
  lattice[2][1].b[1][0] = -1;
  lattice[2][1].b[1][1] = 0;
  
  lattice[2][2].k[0][0] = 0;
  lattice[2][2].k[0][1] = 2;
  lattice[2][2].k[1][0] = 0;
  lattice[2][2].k[1][1] = 1;
  
  beginRecord(PDF, "out/baryon_step2_1.pdf");
  background(255);
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    lattice[t][x].show();
  }
  endRecord();
  
  // baryon move 2_2
  
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    int[] args = {t, x, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    lattice[t][x] = new latticePoint(args);
  }
  
  lattice[0][0].b[0][0] = 0;
  lattice[0][0].b[0][1] = 1;
  lattice[0][0].b[1][0] = -1;
  lattice[0][0].b[1][1] = 0;
  
  lattice[0][1].b[0][0] = -1;
  lattice[0][1].b[0][1] = 1;
  lattice[0][1].b[1][0] = 0;
  lattice[0][1].b[1][1] = 0;
  
  lattice[0][2].b[0][0] = 1;
  lattice[0][2].b[0][1] = -1;
  lattice[0][2].b[1][0] = 0;
  lattice[0][2].b[1][1] = 0;
  
  lattice[1][0].b[0][0] = -1;
  lattice[1][0].b[0][1] = 1;
  lattice[1][0].b[1][0] = 0;
  lattice[1][0].b[1][1] = 0;
  lattice[1][0].head = true;
  
  lattice[1][1].b[0][0] = -1;
  lattice[1][1].b[0][1] = 0;
  lattice[1][1].b[1][0] = 1;
  lattice[1][1].b[1][1] = 0;
  
  lattice[1][2].b[0][0] = 1;
  lattice[1][2].b[0][1] = 0;
  lattice[1][2].b[1][0] = 0;
  lattice[1][2].b[1][1] = -1;
  
  lattice[2][0].b[0][0] = -1;
  lattice[2][0].b[0][1] = 0;
  lattice[2][0].b[1][0] = 0;
  lattice[2][0].b[1][1] = 1;
  
  lattice[2][1].b[0][0] = 0;
  lattice[2][1].b[0][1] = 1;
  lattice[2][1].b[1][0] = -1;
  lattice[2][1].b[1][1] = 0;
  
  lattice[2][2].k[0][0] = 0;
  lattice[2][2].k[0][1] = 2;
  lattice[2][2].k[1][0] = 0;
  lattice[2][2].k[1][1] = 1;
  
  beginRecord(PDF, "out/baryon_step2_2.pdf");
  background(255);
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    lattice[t][x].show();
  }
  endRecord();
  
  // baryon move 3_1
  
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    int[] args = {t, x, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    lattice[t][x] = new latticePoint(args);
  }
  
  lattice[0][0].k[0][0] = 0;
  lattice[0][0].k[0][1] = 0;
  lattice[0][0].k[1][0] = 1;
  lattice[0][0].k[1][1] = 2;
  
  lattice[0][1].k[0][0] = 0;
  lattice[0][1].k[0][1] = 0;
  lattice[0][1].k[1][0] = 2;
  lattice[0][1].k[1][1] = 1;
  
  lattice[0][2].k[0][0] = 2;
  lattice[0][2].k[0][1] = 0;
  lattice[0][2].k[1][0] = 1;
  lattice[0][2].k[1][1] = 0;
  
  lattice[1][0].b[0][0] = 0;
  lattice[1][0].b[0][1] = -1;
  lattice[1][0].b[1][0] = 0;
  lattice[1][0].b[1][1] = 1;
  
  lattice[1][1].b[0][0] = 0;
  lattice[1][1].b[0][1] = 0;
  lattice[1][1].b[1][0] = 0;
  lattice[1][1].b[1][1] = 1;
  lattice[1][1].head = true;
  lattice[1][1].m = 1;
  
  lattice[1][2].b[0][0] = 0;
  lattice[1][2].b[0][1] = 1;
  lattice[1][2].b[1][0] = -1;
  lattice[1][2].b[1][1] = 0;
  
  lattice[2][0].b[0][0] = 1;
  lattice[2][0].b[0][1] = 0;
  lattice[2][0].b[1][0] = 0;
  lattice[2][0].b[1][1] = -1;
  
  lattice[2][1].b[0][0] = 0;
  lattice[2][1].b[0][1] = 0;
  lattice[2][1].b[1][0] = 1;
  lattice[2][1].b[1][1] = -1;
  
  lattice[2][2].b[0][0] = -1;
  lattice[2][2].b[0][1] = 0;
  lattice[2][2].b[1][0] = 1;
  lattice[2][2].b[1][1] = 0;
  
  beginRecord(PDF, "out/baryon_step3_1.pdf");
  background(255);
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    lattice[t][x].show();
  }
  endRecord();
  
  // baryon move 3_1
  
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    int[] args = {t, x, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    lattice[t][x] = new latticePoint(args);
  }
  
  lattice[0][0].k[0][0] = 0;
  lattice[0][0].k[0][1] = 0;
  lattice[0][0].k[1][0] = 1;
  lattice[0][0].k[1][1] = 2;
  
  lattice[0][1].k[0][0] = 0;
  lattice[0][1].k[0][1] = 0;
  lattice[0][1].k[1][0] = 2;
  lattice[0][1].k[1][1] = 1;
  
  lattice[0][2].k[0][0] = 2;
  lattice[0][2].k[0][1] = 0;
  lattice[0][2].k[1][0] = 1;
  lattice[0][2].k[1][1] = 0;
  
  lattice[1][0].b[0][0] = 0;
  lattice[1][0].b[0][1] = -1;
  lattice[1][0].b[1][0] = 0;
  lattice[1][0].b[1][1] = 1;
  
  lattice[1][1].b[0][0] = 0;
  lattice[1][1].b[0][1] = 0;
  lattice[1][1].b[1][0] = -1;
  lattice[1][1].b[1][1] = 1;
  
  lattice[1][2].b[0][0] = 0;
  lattice[1][2].b[0][1] = 1;
  lattice[1][2].b[1][0] = -1;
  lattice[1][2].b[1][1] = 0;
  
  lattice[2][0].b[0][0] = 1;
  lattice[2][0].b[0][1] = 0;
  lattice[2][0].b[1][0] = 0;
  lattice[2][0].b[1][1] = -1;
  
  lattice[2][1].b[0][0] = 0;
  lattice[2][1].b[0][1] = 0;
  lattice[2][1].b[1][0] = 1;
  lattice[2][1].b[1][1] = -1;
  
  lattice[2][2].b[0][0] = -1;
  lattice[2][2].b[0][1] = 0;
  lattice[2][2].b[1][0] = 1;
  lattice[2][2].b[1][1] = 0;
  
  beginRecord(PDF, "out/baryon_step3_2.pdf");
  background(255);
  for(int t = 0; t < N_t; t++)
  for(int x = 0; x < N_x; x++){
    lattice[t][x].show();
  }
  endRecord();
  
  exit();
}

float[] convertToPixels(float x, float y){
  float[] pixels = new float[2];
  pixels[0] = map(x, 0, N_x - 1, borders.x, width - borders.x);
  pixels[1] = map(y, N_t - 1, 0, borders.y, height - borders.y);
  return pixels;
}

void drawMesonLine(float x, float y, int nu, int forward, int k){
  if(k == 0) return;
  float direction = PI * forward + PI/2 * nu;
  float offset = (k - 1) * 0.5 * mesonLineSpacing;
  for(int i = 0; i < k; i++){
    float startX = x - sin(direction) * siteSize + cos(direction) * (mesonLineSpacing * i - offset);
    float startY = y + cos(direction) * siteSize + sin(direction) * (mesonLineSpacing * i - offset);
    float endX = x - sin(direction) * spacing.x/2 + cos(direction) * (mesonLineSpacing * i - offset);
    float endY = y + cos(direction) * spacing.y/2 + sin(direction) * (mesonLineSpacing * i - offset);
    line(startX, startY, endX, endY);
  }
}

void drawBaryonLine(float x, float y, int nu, int forward, int b){
  if(b == 0) return;
  //if(b == 1) return;
  float direction = PI * forward + PI/2 * nu;
  float startX = x - sin(direction) * siteSize;
  float startY = y + cos(direction) * siteSize;
  float endX = x - sin(direction) * spacing.x/2;
  float endY = y + cos(direction) * spacing.y/2;
  line(startX, startY, endX, endY);
  direction += PI/2;
  PVector midpoint = new PVector(map(0.5, 0, 1, startX, endX), map(0.5, 0, 1, startY, endY));
  PVector point1 = new PVector(midpoint.x + (sin(direction) + cos(direction) * b) * baryonArrowSize, midpoint.y - (cos(direction) - sin(direction) * b) * baryonArrowSize) ;
  PVector point2 = new PVector(midpoint.x + (-sin(direction) + cos(direction) * b) * baryonArrowSize, midpoint.y - (-cos(direction) - sin(direction) * b) * baryonArrowSize) ;
  PVector point3 = new PVector(midpoint.x - (cos(direction) * b) * baryonArrowSize, midpoint.y - sin(direction) * b * baryonArrowSize) ;
  fill(0);
  triangle(point1.x, point1.y, point2.x, point2.y, point3.x, point3.y);
}
