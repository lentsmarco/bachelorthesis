import processing.pdf.*;

int gridsize = 10;
int gridheight = 3;
PVector perspective = new PVector(0, 0.5, 460);

void setup(){
  size(1400, 1400);
}

void draw(){
  beginRecord(PDF, "out/layout.pdf");
  background(255);
  background(255);
  textAlign(CENTER, CENTER);
  float cellwidth = width / gridsize*0.65;
  float cellheight = height / gridsize*0.65;
  for(int z = gridheight-1; z >= 0; z--)
  for(int x = 0; x < gridsize; x++)
  for(int y = 0; y < gridsize; y++){
   fill(255);
   beginShape();
   vertex(x * cellwidth + y * cellheight * perspective.y, y * cellheight * perspective.y + z * perspective.z);
   vertex((x+1) * cellwidth + y * cellheight * perspective.y, y * cellheight * perspective.y + z * perspective.z);
   vertex((x+1) * cellwidth + (y+1) * cellheight * perspective.y, (y+1) * cellheight * perspective.y + z * perspective.z);
   vertex(x * cellwidth + (y+1) * cellheight * perspective.y, (y+1) * cellheight * perspective.y + z * perspective.z);
   endShape(CLOSE);
   fill(0);
   text(x+y*gridsize + (gridheight-z-1)*gridsize*gridsize ,(x+0.5) * cellwidth + (y+0.5) * cellwidth * perspective.y, (y+0.5) * cellwidth * perspective.y+ z * perspective.z);
  }
  endRecord();
  exit();
}
