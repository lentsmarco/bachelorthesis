% !TeX spellcheck = en_GB
\section{Monte Carlo Methods}
The worm algorithm, which is the main focus of this thesis, belongs to a class of algorithms called \textit{Monte Carlo Methods}, where one can find good estimates for calculations using randomness to some extent.
\subsection{Approximating integrals using Monte Carlo Methods}
In the end, we want to use the worm algorithm to approximate the QCD partition function \autoref{partition function}. However we will first focus on a simpler example, to make things more clear. In order to find the integral of any function $\int_a^b f(x)\cdot g (x)\dif x$, it is usually divided into $N$ rectangles, whose area is summed up and then the limit $N\rightarrow\infty$ is taken. However there is an alternative way of producing the same result: we could also choose a random $x$ between $a$ and $b$ with probability density $f$ and evaluate $g$ at $x$. The expected value of $g$ with density $f$ (written as $\langle g\rangle_f$) turns out to be $\langle g\rangle_f=\int_a^b f(x)\cdot g(x)\dif x$.
The worm algorithm will calculate observables by approximating
\begin{equation}
	\langle A\rangle=\frac{1}{Z}\sum_{\{k,n,l\}}A(k,n,l)\prod_{b=(x,\hat{\mu})}\frac{(3-k_b)!}{3!k_b!}\gamma^{2k_b\delta_{\nu,0}}\prod_x\frac{3!}{n_x!}(2m_q)^{n_x}\prod_lw(l).
\end{equation}
Since $w(l)$ can be negative, we generate configurations $(k,n,l)$ with probability density\\ $\frac{1}{Z}\prod_{b=(x,\hat{\mu})}\frac{(3-k_b)!}{3!k_b!}\gamma^{2k_b\delta_{\nu,0}}\prod_x\frac{3!}{n_x!}(2m_q)^{n_x}\prod_l|w(l)|$ (note the absolute value around $w(l)$) and calculate $\frac{\langle A\sigma\rangle}{\langle\sigma\rangle}$, with $\sigma=\prod_l\frac{w(l)}{|w(l)|}$ (compare \autoref{worm formulation}).
\subsection{Markov chains} \label{Markov chains}
It is however not trivial to generate configurations with the right probability, since the partition function $Z$ is not known explicitly, so an algorithm is used: we start at some allowed configuration $\mathcal{C}=(k, n, l)$. Then we transition to a new state with transition probability $P(\mathcal{C}\rightarrow\mathcal{C}')$. The last step is repeated until we have generated a sufficient number of configurations. If the transition probability $P(\mathcal{C}_a\rightarrow\mathcal{C}_b)$ only depends on the states $\mathcal{C}_a$ and $\mathcal{C}_b$, the process is called a \textit{Markov chain} and can be modelled by multiplying a state vector $\pi(n)=\begin{pmatrix}
	\pi_1(n)\\
	\pi_2(n)\\
	\vdots
\end{pmatrix}$
by a transition matrix $T=\begin{pmatrix}
	P(\mathcal{C}_1\rightarrow\mathcal{C}_1)&P(\mathcal{C}_2\rightarrow\mathcal{C}_1)&\dots\\
	P(\mathcal{C}_2\rightarrow\mathcal{C}_1)&P(\mathcal{C}_2\rightarrow\mathcal{C}_2)&\dots\\
	\vdots&\vdots&\ddots
\end{pmatrix}$
, so that $\pi_{n+1}=T\pi_n$. The $\pi_i(n)$ correspond to the probability of being in state $\mathcal{C}_i$ at step $n$.\\\\
If the transition matrix fulfils 
\begin{enumerate}
	\item \textit{reversibility} with respect to some state vector $\pi^*$, meaning that $\pi_a^*\cdot P(\mathcal{C}_a\rightarrow\mathcal{C}_b)=\pi_b^*\cdot P(\mathcal{C}_b\rightarrow\mathcal{C}_a)$ or in matrix form $\Pi T = T^T \Pi$ with $\Pi=\text{diag}(\pi_1^*,\pi_2^*,\dots)$. This is also called the \textit{detailed balance} condition.
	\item \textit{ergodicity}, meaning that $\forall i,j \exists k: (T^k)_{i,j}\neq0$. Conceptually this means that every state is reachable by any other state after $k$ steps.
\end{enumerate}
then $\pi^*$ is the unique \textit{stationary probability distribution} with $T\pi^*=\pi^*$ (i.e. an eigenvector of $T$ with eigenvalue $1$) and all other eigenvalues are smaller than $1$\cite{lqcd}.\\
This means that for any starting $\pi(0)$, $\lim_{k\rightarrow\infty}T^k\pi(0)=\pi^*$. The phase until the stationary distribution is sufficiently approached is called \textit{thermalization}. Note that we are able to freely choose this stationary distribution by selecting $T$ according to the detailed balance condition.\\
If we now choose $T$ like so, we achieve $\pi^*_i =W(\mathcal{C}_i):=\frac{1}{Z}\prod_{b=(x,\hat{\mu})}\frac{(3-k_{b,i})!}{3!k_{b,i}!}\gamma^{2k_{b,i}\delta_{\nu,0}}\prod_x\frac{3!}{n_{x,i}!}(2m_q)^{n_{x,i}}\prod_l|w(l_i)|$.\\
One possible choice for a transition probability to generate an arbitrary stationary distribution $\pi^*$ is the \textit{Metropolis} algorithm, which consists of two steps:
\begin{enumerate}
	\item \textit{proposal}: a new state $\mathcal{C}_j$ is proposed with some probability $P_{\text{prop}}(\mathcal{C}_i\rightarrow \mathcal{C}_j)$. One possible choice in our case would be to consider all configurations with two changed link variables with equal probability.
	\item \textit{acceptance}: the proposal is accepted with probability $P_{\text{acc}}(\mathcal{C}_i\rightarrow \mathcal{C}_j)=\min(1,\frac{\pi^*_jP_{\text{prop}}(\mathcal{C}_j\rightarrow \mathcal{C}_i)}{\pi^*_iP_{\text{prop}}(\mathcal{C}_i\rightarrow \mathcal{C}_j)})$.
\end{enumerate}
If the proposal is not accepted, nothing is changed, so a big problem of this algorithm is, that it may take quite some time until the system is thermalized. This algorithm is still very useful, because we get the same stationary probability distribution, even if $\pi^*$ is not properly normalized, since constants cancel out in the acceptance step. This means that the unknown factor $\frac{1}{Z}$ in $W(\mathcal{C}_i)$ can be ignored when calculating transition probabilities.\\
The worm algorithm uses a different choice of transition probabilities where there is also a proposed change, however it is ensured, that at least every $2d$th proposal is accepted, where $d$ is the dimensionality of the grid (see \autoref{algorithm} and \autoref{probabilities}).