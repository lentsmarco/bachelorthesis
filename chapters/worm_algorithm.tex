% !TeX spellcheck = en_GB
\section{Worm algorithm}
The original source code can be viewed at\\ \url{https://gitlab.com/lentsmarco/qcd/-/tree/f871a60f02485ddebe7e18079045baeabb5d4617}, the algorithm in action can be seen at \url{https://youtu.be/o4EgL94hxfo}.
\subsection{Visualization of the configurations}
To better understand how the configurations should be interpreted, this formulation allows a nice way to visualize what is going on. The possible states that a link can be in are shown in \autoref{fig:configs}. The three links on the left correspond to dimers with $k=1,2,3$, whereas the one on the right corresponds to a baryonic link with $b=\pm1$.
\begin{figure}[h]
	\begin{subfigure}{.5\textwidth}
		\includegraphics[height=8cm]{images/configurations/out/links.pdf}
		\caption{possible link states}
		\label{fig:configs}
	\end{subfigure}
	\begin{subfigure}{.5\textwidth}
		\includegraphics[height=8cm]{images/configurations/out/starting_conf.pdf}
		\caption{starting configuration}
		\label{fig:starting conf}
	\end{subfigure}
	\caption{visualization of links}
\end{figure}
This visualization will become useful, when we start examining the worm algorithm, which will define transitions between configurations. We will start by initializing the lattice in the state, that can be seen in \autoref{fig:starting conf}, where wrapping around the edge of the figure is implied. As can be easily verified, every site satisfies $\sum_{\hat{\mu}}k_{\hat{\mu}}=3$.
\subsection{Algorithmic description}\label{algorithm}
It should be noted that during a worm update, we allow the link variables to \enquote{desynchronize}, meaning that $k_{x,\hat{\mu}}$ need not necessarily equal $k_{y,-\hat{\mu}}$ and similarly for the $b$. This is however only allowed when the link is updated, so after the worm closes all link variables are in sync again.
\subsubsection{Partitioning of the lattice}
It is convenient to partition the sites into active $x_a$ and passive $x_p$ sites so every link has exactly one active and one passive site. This allows us to rewrite the partition function as 
\begin{equation}
	Z=\sum_{\{n,k,b\}}\prod_{x_a}W_a(x_a)\prod_{x_p}W_p(x_p)\sigma(\{n,k,b\}),
\end{equation}
with
\begin{align}
	\label{weights active}
	W_a(x)&=\frac{3!}{n_x!}(2m)^{n_x}\prod_{\hat{\nu}=\pm\hat{0},\dots,\pm\hat{d}}\frac{(3-k_{\hat{\nu}})!}{3!k_{\hat{\nu}}!}\frac{1}{(3!)^{|b_{\hat{\nu}}|}}\\
	\label{weights passive}
	W_p(x)&=\frac{3!}{n_x!}(2m)^{n_x}\prod_{\hat{\nu}=\pm\hat{0}}\gamma^{2k_{\hat{\nu}}+3|b_{\hat{\nu}}|}\exp\left[3b_{\hat{\nu}}(\delta_{\hat{\nu},\hat{0}}-\delta_{\hat{\nu},-\hat{0}})\mu\right].
\end{align}
\newpage
\subsubsection{Mesonic worm update}
In this part of the algorithm, only mesonic degrees of freedom are altered. The directions $\mu$ and $\rho$ are to be understood identical between successive moves if the move is in direction $\mu$ or $\rho$, otherwise not.

The algorithm then works as follows:
\begin{enumerate}
	\item Choose a site $x$ with uniform probability that is not part of a baryon loop. We define $x$ to be an active site; this defines the entire set of active sites to be those with the same parity $\epsilon(x')=(-1)^{\sum_{i=0}^d x'_i}$ as $x$. We now place the worm head and tail at $x$ in terms of two monomers. To fulfil the constraint $n_x+\sum_{\hat{\nu}}k_{\hat{\nu}}(x)=3$, we now need to separate the head from the tail, by moving to a neighbour site and erasing a dimer in this direction. (To avoid unnecessary changes in monomer numbers, we do not actually change the monomer number at the worm head,  but just remember that it stands for a monomer when closing the worm) We thus choose a direction with probability \begin{equation}
		P_{\hat{\mu}} = \frac{k_{\hat{\mu}}}{3}
	\end{equation}(see \autoref{probabilities}). Because we do not have any monomers yet and $n_x+\sum_{\hat{\nu}}k_{\hat{\nu}}(x)=3$ we are guaranteed to choose some direction. To end this step, we move to site $y=x+\hat{\mu}$ and update 
	\begin{itemize}
		\item $n_x\rightarrow n_x+1$
		\item $k_{\hat{\mu}}(x)\rightarrow k_{\hat{\mu}}(x)-1$.
	\end{itemize}
	
	\item By definition the site $y$ is passive, so we now choose a direction $\hat{\rho}$ with probability
	\begin{equation}
		P_{\hat{\rho}}=\frac{W_{\hat{\rho}}}{W_D(y)},\qquad W_{\hat{\rho}}=
		\left\{\begin{array}{ll}
			0 & y+\hat{\rho}\text{ is member of baryon loop}\\
			\gamma^{2\delta_{\hat{\rho}\pm\hat{0}}} & \text{otherwise} \\
		\end{array}\right.
	,\qquad W_D(x)=\sum_{\hat{\nu}}W_{\hat{\nu}}.
	\end{equation}
	Now we move to site $z=y+\hat{\rho}$ and update
	\begin{itemize}
		\item $k_{-\hat{\mu}}(y)\rightarrow k_{-\hat{\mu}}(y)-1$
		\item $k_{\hat{\rho}}(y)\rightarrow k_{\hat{\rho}}(y)+1$.
	\end{itemize}
	\item In this step we avoid backtracking by not considering steps in direction $-\hat{\rho}$, which forces us to adjust the denominator of the probabilities to $3-k_{-\hat{\rho}}$.\\
	If $z=x$, we now have the ability to close the worm by removing the monomer we left at step 1. We choose this possibility with probability
	\begin{equation}
		P_z=\frac{n_z}{3-k_{-\hat{\rho}}}.
	\end{equation}
	If this is accepted, we update
	\begin{itemize}
		\item $n_z\rightarrow n_z-1$
		\item $k_{\hat{\rho}} \rightarrow k_{\hat{\rho}}+1$
	\end{itemize}
	and end the mesonic worm update.\\
	Otherwise, we continue the path in direction $\hat{\mu}\neq-\hat{\rho}$ with probability
	\begin{equation}
		P_\mu=\frac{k_{\hat{\mu}}}{3-k_{-\hat{\rho}}},
	\end{equation}
	update
	\begin{itemize}
		\item $k_{-\hat{\rho}}\rightarrow k_{-\hat{\rho}} +1$
		\item $k_{\hat{\mu}}\rightarrow k_{\hat{\mu}} -1$
	\end{itemize}
	and continue with step 2.
	\newpage
\end{enumerate}
\subsubsection{Baryonic worm update}
In order to achieve ergodicity, we need the ability to turn mesonic degrees of freedom into baryonic ones and vice versa. In this part of the algorithm this is achieved by turning triple dimers into baryonic links along with the reverse \cite{fromm}.\\
The algorithm works as follows
\begin{enumerate}
	\item Once again choose a site $x$ with uniform probability which is connected to a triple dimer or is part of a baryonic loop. We, again, define the set of active sites as those with the same parity as $x$. Now, depending on $x$, we continue by either
	\begin{enumerate}[label=\alph*)]
		\item $x$ is member of a baryon loop, which means there is one incoming ($b_{\hat{\mu}}=1$) and one outgoing ($b_{\hat{\mu}}=-1$) baryonic link. In this case we move to the neighbour site in the outgoing direction without changing the mesonic degrees of freedom.
	\end{enumerate}
	or
	\begin{enumerate}[resume*]
		\item $x$ has a triple dimer in direction $\hat{\mu}$. In this case we set $k_{\hat{\mu}}=0$.
	\end{enumerate}
	we additionally update $b_{\hat{\mu}}\rightarrow b_{\hat{\mu}}+1$ before we move to $y=x+\hat{\mu}$.
	\item We arrive at the passive site $y$. Similarly to the mesonic second step, we choose a direction $\hat{\rho}$ with probability
	\begin{equation}
		P_{\hat{\rho}}=\frac{W_{\hat{\rho}}}{W_D(y)},\hspace{0.2cm} W_{\hat{\rho}}=
		\left\{\begin{array}{ll}
			\gamma^{3\delta_{\hat{\rho}\pm\hat{0}}}\exp(3\mu(\delta_{\hat{\rho},+\hat{0}}-\delta_{\hat{\rho},-\hat{0}})) & \substack{\text{y}+\hat{\rho}\text{ is member of a baryon loop}\\\text{or is touched by a triple dimer}} \\
			0 & \text{otherwise}\\
		\end{array}\right.
		,\hspace{0.2cm} W_D(x)=\sum_{\hat{\nu}}W_{\hat{\nu}},
	\end{equation}
	update 
	\begin{itemize}
		\item $b_{-\hat{\mu}}\rightarrow b_{-\hat{\mu}}-1$
		\item $k_{-\hat{\mu}}=0$
		\item $b_{\hat{\rho}}\rightarrow b_{\hat{\rho}}+1$
		\item If $b_{\hat{\rho}}=0$ now, then $k_{\hat{\rho}}=3$
	\end{itemize}
	and move to $z=y+\hat{\rho}$.
	\item for site $z$ there are three different options
	\begin{itemize}
		\item If $z$ is the site we started at in step 1 the update ends and we update
		\begin{itemize}
			\item $b_{-\hat{\rho}}\rightarrow b_{-\hat{\rho}}-1$
			\item if $b_{-\hat{\rho}}=0$ now, then $k_{-\hat{\rho}}=3$.
		\end{itemize}
		\item If $z$ is touched by a triple dimer in direction $\hat{\mu}$, we update
		\begin{itemize}
			\item $b_{-\hat{\rho}}\rightarrow b_{-\hat{\rho}}-1$
			\item $b_{\hat{\mu}}=1$
			\item $k_{\hat{\mu}}=0$
		\end{itemize}
		and continue with step 2.
		\item If $z$ is a member of a baryon loop ($\Rightarrow$ there is a direction $\hat{\mu}$ with $b_{\hat{\mu}}=-1$), we update
		\begin{itemize}
			\item $b_{-\hat{\rho}} \rightarrow b_{-\hat{\rho}}-1$
			\item $b_{\hat{\mu}}=0$
			\item if $b_{-\hat{\rho}}=0$ now, then $k_{-\hat{\rho}}=3$
		\end{itemize}
		and continue with step 2.
	\end{itemize}
\end{enumerate}
The two parts of the algorithm can be performed with different frequencies in order to thermalize the system faster.
\subsubsection{Visualization of the steps}
In \autoref{meson move visualization} and \autoref{baryon move visualization} some examples for the possible moves and their effect can be seen. In the figures a site colored in red has a meson source $n_x=1$, a site colored in blue has a baryon source and the site with the worm head is colored in green. The site where the step starts is always in the center of the figure and there is no wrapping around the edges implied.
\begin{figure}[H]
	\centering
	\newcommand{\imagesize}{5cm}
	\newcommand{\horizontalPadding}{0.2cm}
	\newcommand{\verticalPadding}{0.5cm}
	\begin{subfigure}{\textwidth}
		\centering
		$\vcenter{\hbox{\frame{\includegraphics[height=\imagesize]{images/configurations/out/meson_step1_1.pdf}}}}\hspace{\horizontalPadding}
		{\Huge\xrightarrow{}}\hspace{\horizontalPadding}
		\vcenter{\hbox{\frame{\includegraphics[height=\imagesize]{images/configurations/out/meson_step1_2.pdf}}}}$
		\caption{meson worm step 1}
		\label{fig:mesons1}
		\vspace{\verticalPadding}
	\end{subfigure}
	\begin{subfigure}{\textwidth}
		\centering
		$\vcenter{\hbox{\frame{\includegraphics[height=\imagesize]{images/configurations/out/meson_step2_1.pdf}}}}\hspace{\horizontalPadding}
		{\Huge\xrightarrow{}}\hspace{\horizontalPadding}
		\vcenter{\hbox{\frame{\includegraphics[height=\imagesize]{images/configurations/out/meson_step2_2.pdf}}}}$
		\caption{meson worm step 2}
		\label{fig:mesons2}
		\vspace{\verticalPadding}
	\end{subfigure}
	\begin{subfigure}{\textwidth}
		\centering
		$\vcenter{\hbox{\frame{\includegraphics[height=\imagesize]{images/configurations/out/meson_step3_1.pdf}}}}\hspace{\horizontalPadding}
		{\Huge\xrightarrow{}}\hspace{\horizontalPadding}
		\vcenter{\hbox{\frame{\includegraphics[height=\imagesize]{images/configurations/out/meson_step3_2.pdf}}}\vspace{0.1cm}}$
		$\vcenter{\hbox{\frame{\includegraphics[height=\imagesize]{images/configurations/out/meson_step3_1.pdf}}}}\hspace{\horizontalPadding}
		{\Huge\xrightarrow{}}\hspace{\horizontalPadding}
		\vcenter{\hbox{\frame{\includegraphics[height=\imagesize]{images/configurations/out/meson_step3_3.pdf}}}\vspace{0.1cm}}$
		\caption{meson worm step 3}
		\label{fig:mesons3}
	\end{subfigure}
	\caption{visualization of meson moves}
	\label{meson move visualization}
\end{figure}
\begin{figure}[H]
	\centering
	\newcommand{\imagesize}{5.5cm}
	\newcommand{\horizontalPadding}{0.2cm}
	\begin{subfigure}{\textwidth}
		\centering
		$\vcenter{\hbox{\frame{\includegraphics[height=\imagesize]{images/configurations/out/baryon_step1_1.pdf}}}}\hspace{\horizontalPadding}
		{\Huge\xrightarrow{}}\hspace{\horizontalPadding}
		\vcenter{\hbox{\frame{\includegraphics[height=\imagesize]{images/configurations/out/baryon_step1_2.pdf}}}\vspace{0.1cm}}$
		$\vcenter{\hbox{\frame{\includegraphics[height=\imagesize]{images/configurations/out/baryon_step1_3.pdf}}}}\hspace{\horizontalPadding}
		{\Huge\xrightarrow{}}\hspace{\horizontalPadding}
		\vcenter{\hbox{\frame{\includegraphics[height=\imagesize]{images/configurations/out/baryon_step1_4.pdf}}}\vspace{0.1cm}}$
		\caption{baryon worm step 1}
		\label{fig:baryons1}
	\end{subfigure}
	\begin{subfigure}{\textwidth}
		\centering
		$\vcenter{\hbox{\frame{\includegraphics[height=\imagesize]{images/configurations/out/baryon_step2_1.pdf}}}}\hspace{\horizontalPadding}
		{\Huge\xrightarrow{}}\hspace{\horizontalPadding}
		\vcenter{\hbox{\frame{\includegraphics[height=\imagesize]{images/configurations/out/baryon_step2_2.pdf}}}}$
		\caption{baryon worm step 2}
		\label{fig:baryons2}
	\end{subfigure}
	\begin{subfigure}{\textwidth}
		\centering
		$\vcenter{\hbox{\frame{\includegraphics[height=\imagesize]{images/configurations/out/baryon_step3_1.pdf}}}}\hspace{\horizontalPadding}
		{\Huge\xrightarrow{}}\hspace{\horizontalPadding}
		\vcenter{\hbox{\frame{\includegraphics[height=\imagesize]{images/configurations/out/baryon_step3_2.pdf}}}\vspace{0.1cm}}$
		\caption{baryon worm step 3}
		\label{fig:baryons3}
	\end{subfigure}
	\caption{visualization of meson moves}
	\label{baryon move visualization}
\end{figure}

\subsection{Proving detailed balance}
As stated in \autoref{Markov chains}, in order to approach the desired distribution of states the algorithm has to fulfil the \textit{detailed balance} condition $\pi^*_a\cdot P(\mathcal{C}_a\rightarrow\mathcal{C}_b)=\pi^*_b\cdot P(\mathcal{C}_b\rightarrow\mathcal{C}_a)$ for all states $\mathcal{C}_a$ and $\mathcal{C}_b$. Multiplying both sides by the partition function $Z$, the equation becomes
\begin{equation}
	W(\mathcal{C}_a)\cdot P(\mathcal{C}_a\rightarrow\mathcal{C}_b)=W(\mathcal{C}_b)\cdot P(\mathcal{C}_b\rightarrow\mathcal{C}_a),
\end{equation}
with weights $W(\mathcal{C}) = \prod_{x_a}W_a(x_a)\prod_{x_p}W_p(x_p)$ and individual site weights of active sites $x_a$ and passive sites $x_p$ respectively given by \autoref{weights active} and \autoref{weights passive}.
\subsubsection{Mesonic worm}
To keep things simple, we start by examining neither the beginning nor the end of the worm but by looking at step 2 and 3, while step 3 does not close the worm. Only then we look at the complete behaviour from start to finish.
\begin{itemize}
	\item Step 2: Let $y$ be the passive site where the update takes place and $\hat{\mu}$ be the incoming direction of the worm. Further, let $\hat{\rho}$ be the outgoing direction of the worm. Since only the site $y$ is modified, the condition reduces to
	\begin{align}
		W_p(y)P_{\hat{\mu}\hat{\rho}}&=W'_p(y)P_{\hat{\rho}\hat{\mu}}\\
		\prod_{\hat{\kappa}}(\gamma^2)^{\delta_{\kappa,0}k_{\hat{\kappa}}}P_{\hat{\mu}\hat{\rho}}&=\prod_{\hat{\kappa}}(\gamma^2)^{\delta_{\kappa,0}k'_{\hat{\kappa}}}P_{\hat{\rho}\hat{\mu}},
	\end{align}
	where $k'$ are the link variables after the update and $P_{\hat{\rho}\hat{\mu}}$ is the probability of the reverse step, i.e. incoming direction $\hat{\rho}$ and outgoing $\hat{\mu}$. Additionally, $P_{\hat{\mu}\hat{\rho}}$ has to satisfy $\sum_{\hat{\nu}}P_{\hat{\mu}\hat{_\nu}}=1$, which can be easily achieved by normalizing the probabilities. During step 2, we update $k_{\hat{\mu}}\rightarrow k_{\hat{\mu}}-1=k'_{\hat{\mu}}$ and $k_{\hat{\rho}}\rightarrow k_{\hat{\rho}}+1=k'_{\hat{\rho}}$. We can check that 
	\begin{equation}
		P_{\hat{\mu}\hat{\nu}}=\frac{W_{\hat{\mu}\hat{\nu}}}{W_D^{\hat{\mu}}(y)},\qquad W_D^{\hat{\mu}}(y)=\sum_{\hat{\kappa}}W_{\hat{\mu}\hat{\kappa}}
	\end{equation}
	with $W_{\hat{i}\pm\hat{0}}=\gamma^2$, $W_{\hat{i}\hat{j}}=1$ ($\hat{i}=\pm0,\dots,\pm d$, $\hat{j}=\pm 1,\dots,\pm d$), but with $W_{\hat{\mu}\hat{\rho}}=0$ for baryonic neighbours $y+\hat{\rho}$ fulfils detailed balance:
	\begin{align}
		\prod_{\hat{\kappa}}(\gamma^2)^{\delta_{\kappa,0}k_{\hat{\kappa}}}P_{\hat{\mu}\hat{\rho}}&=\prod_{\hat{\kappa}}(\gamma^2)^{\delta_{\kappa,0}k'_{\hat{\kappa}}}P_{\hat{\rho}\hat{\mu}}\\
		\prod_{\hat{\kappa}}(\gamma^2)^{\delta_{\kappa,0}k_{\hat{\kappa}}}\frac{W_{\hat{\mu}\hat{\rho}}}{W_D^{\hat{\mu}}(y)}&=\prod_{\hat{\kappa}}(\gamma^2)^{\delta_{\kappa,0}k'_{\hat{\kappa}}}\frac{W_{\hat{\rho}\hat{\mu}}}{{W'}_D^{\hat{\rho}}(y)}.
	\end{align}
	Note that $W_D^{\hat{\mu}}={W'}_D^{\hat{\rho}}(y)$.
	Now we need to distinguish three cases
	\begin{enumerate}
		\item \label{meson detailed balance case 1} $\hat{\mu}\neq \pm\hat{0}\neq\hat{\rho}\Rightarrow$ no $k$s in time direction change, so $W_p(y)=W'_p(y)$. This means that the detailed balance condition simplifies to
		\begin{align}
			P_{\hat{\mu}\hat{\rho}}&=P_{\hat{\rho}\hat{\mu}}\\
			W_{\hat{\mu}\hat{\rho}}&=W_{\hat{\rho}\hat{\mu}}\\
			1&=1
		.\end{align}
		\item $\hat{\mu}=\pm\hat{0}\neq\hat{\rho}\Rightarrow k'_{\hat{\mu}}=k_{\hat{\mu}}-1\Rightarrow  W_p(y)=\gamma^2 W'_p(y)$. The condition now becomes
		\begin{align}
			W_p(y)P_{\hat{\mu}\hat{\rho}}&=W'_p(y)P_{\hat{\rho}\hat{\mu}}\\
			\gamma^2 P_{\hat{\mu}\hat{\rho}}&=P_{\hat{\rho}\hat{\mu}}\\
			\gamma^2 W_{\hat{\mu}\hat{\rho}}&=W_{\hat{\rho}\hat{\mu}}\\
			\gamma^2&=\gamma^2
		.\end{align}
		\item $\hat{\mu}=\pm\hat{0}, \hat{\rho}=\pm\hat{0}\Rightarrow k'_{\hat{\mu}}=k_{\hat{\mu}}-1, k'_{\hat{\rho}}=k_{\hat{\rho}}+1\Rightarrow \gamma^2W_p(y)=\gamma^2 W'_p(y)$ analogous to \autoref{meson detailed balance case 1}.
	\end{enumerate}
	It is noteworthy that it is not possible to come from a baryonic site, therefore detailed balance for directions from and to baryonic sites is fulfilled by setting this probability to $0$.
	\item Step 3: Let $x$ be the active site and $\hat{\mu}$ be the incoming direction and of the worm. Further let $\hat{\rho}$ be the outgoing direction of the worm. The condition then reads
	\begin{align}
		W_a(x)P_{\hat{\mu}\hat{\rho}}&=W'_a(x)P_{\hat{\rho}\hat{\mu}}\\
		\prod_{\hat{\kappa}}\left(\frac{(3-k_{\hat{\kappa}})!}{3!k_{\hat{\kappa}}!}\right)\frac{3!}{n_x!}(2m)^{n_x}P_{\hat{\mu}\hat{\rho}}&=\prod_{\hat{\kappa}}\left(\frac{(3-k'_{\hat{\kappa}})!}{3!k'_{\hat{\kappa}}!}\right)\frac{3!}{n'_x!}(2m)^{n'_x}P_{\hat{\rho}\hat{\mu}}
	.\end{align}
	Since we are not going to end the worm by picking up the meson source $n_x=n'_x$. Also $k_{\hat{\mu}}\rightarrow k_{\hat{\mu}}+1=k'_{\hat{\mu}}$ and $k_{\hat{\rho}}\rightarrow k_{\hat{\rho}}-1=k'_{\hat{\rho}}$ we get
	\begin{align}
		W_a(x)P_{\hat{\mu}\hat{\rho}}&=W'_a(x)P_{\hat{\rho}\hat{\mu}}\\
		\prod_{\hat{\kappa}}\left(\frac{(3-k_{\hat{\kappa}})!}{3!k_{\hat{\kappa}}!}\right)P_{\hat{\mu}\hat{\rho}}&=\prod_{\hat{\kappa}}\left(\frac{(3-k'_{\hat{\kappa}})!}{3!k'_{\hat{\kappa}}!}\right)P_{\hat{\rho}\hat{\mu}}\\
		\left(\frac{(3-k_{\hat{\mu}})!}{3!k_{\hat{\mu}}!}\frac{(3-k_{\hat{\rho}})!}{3!k_{\hat{\rho}}!}\right)P_{\hat{\mu}\hat{\rho}}&=	\left(\frac{(3-k'_{\hat{\mu}})!}{3!k'_{\hat{\mu}}!}\frac{(3-k'_{\hat{\rho}})!}{3!k'_{\hat{\rho}}!}\right)P_{\hat{\rho}\hat{\mu}}\\
		\left(\frac{(3-k_{\hat{\mu}})!}{3!k_{\hat{\mu}}!}\frac{(3-k_{\hat{\rho}})!}{3!k_{\hat{\rho}}!}\right)P_{\hat{\mu}\hat{\rho}}&=	\left(\frac{(3-(k_{\hat{\mu}}+1))!}{3!(k_{\hat{\mu}}+1)!}\frac{(3-(k_{\hat{\rho}}-1))!}{3!(k_{\hat{\rho}}-1)!}\right)P_{\hat{\rho}\hat{\mu}}\\
		\left(\frac{(3-k_{\hat{\mu}})!}{3!k_{\hat{\mu}}!}\frac{(3-k_{\hat{\rho}})!}{3!k_{\hat{\rho}}!}\right)P_{\hat{\mu}\hat{\rho}}&=	\left(\frac{(3-k_{\hat{\mu}}-1)!}{3!(k_{\hat{\mu}}+1)!}\frac{(3-k_{\hat{\rho}}+1)!}{3!(k_{\hat{\rho}}-1)!}\right)P_{\hat{\rho}\hat{\mu}}\\ 
		\frac{3-k_{\hat{\mu}}}{k_{\hat{\rho}}}P_{\hat{\mu}\hat{\rho}}&=\frac{3-k_{\hat{\rho}}+1}{k_{\hat{\mu}}+1}P_{\hat{\rho}\hat{\mu}}\\
		\frac{3-k_{\hat{\mu}}}{k_{\hat{\rho}}}P_{\hat{\mu}\hat{\rho}}&=\frac{3-k'_{\hat{\rho}}}{k'_{\hat{\mu}}}P_{\hat{\rho}\hat{\mu}}
	.\end{align}
	Therefore a natural choice for $P_{\hat{\mu}\hat{\rho}}$ is $\frac{k_{\hat{\rho}}}{3-k_{\hat{\mu}}}$, for which detailed balance is obviously met. If we additionally set $P_{\hat{\mu}\hat{\mu}}=0$, we get $\sum_{\hat{\nu}}P_{\hat{\mu}\hat{\nu}}+P_{\hat{\mu}x}=1$ where $P_{\hat{\mu}x}=\frac{n_x}{3-k_{\hat{\mu}}}$ is the probability to erase a monomer. This nicely excludes backtracking, i.e. $\hat{\mu}\neq\hat{\rho}$.
	\item Step 1 and Step 3 closing the worm: step 1 is identical to step 3 if one considers there to be an extra direction $n_x$ from where the worm head comes in the beginning and where it eventually ends. Hence the inverse move to step 1 is step 3, when picking the meson source back up. The variables change as follows: $n_x\rightarrow n_x+1=n'_x$, $k_{\hat{\mu}}\rightarrow k_{\hat{\mu}}-1=k'_{\hat{\mu}}$.\\
	Let $x$ be the active site and $\hat{\mu}$ be the outgoing (incoming) direction of the worm in step 1 (step 3). We get
	\begin{align}
		\frac{1}{k_{\hat{\mu}}}P_{x\hat{\mu}}&=\frac{3-k'_{\hat{\mu}}}{n'_x}2mP_{\hat{\mu}x}\\
		\prod_{\hat{\kappa}}\left(\frac{(3-k_{\hat{\kappa}})!}{3!k_{\hat{\kappa}}!}\right)\frac{3!}{n_x!}(2m)^{n_x}P_{x\hat{\mu}}&=\prod_{\hat{\kappa}}\left(\frac{(3-k'_{\hat{\kappa}})!}{3!k'_{\hat{\kappa}}!}\right)\frac{3!}{n'_x!}(2m)^{n'_x}P_{\hat{\mu}x}\\
		\left(\frac{(3-k_{\hat{\mu}})!}{3!k_{\hat{\mu}}!}\right)\frac{3!}{n_x!}(2m)^{n_x}P_{x\hat{\mu}}&=\left(\frac{(3-k'_{\hat{\mu}})!}{3!k'_{\hat{\mu}}!}\right)\frac{3!}{n'_x!}(2m)^{n'_x}P_{\hat{\mu}x}\\
		\left(\frac{(3-k_{\hat{\mu}})!}{3!k_{\hat{\mu}}!}\right)\frac{3!}{n_x!}(2m)^{n_x}P_{x\hat{\mu}}&=\left(\frac{(3-(k_{\hat{\mu}}-1))!}{3!(k_{\hat{\mu}}-1)!}\right)\frac{3!}{(n_x+1)!}(2m)^{n_x+1}P_{\hat{\mu}x}\\
		\frac{1}{k_{\hat{\mu}}}P_{x\hat{\mu}}&=\frac{3-k_{\hat{\mu}}+1}{n_x+1}2mP_{\hat{\mu}x}\\
		\frac{1}{k_{\hat{\mu}}}P_{x\hat{\mu}}&=\frac{3-k'_{\hat{\mu}}}{n'_x}2mP_{\hat{\mu}x}
	.\end{align}
	Inserting $P_{\hat{\mu}x}=\frac{n_x}{3-k_{\hat{\mu}}}$ and $P_{x\hat{\mu}}=\frac{k_{\hat{\mu}}}{3 V_D}$ (the factor $V_D$ appears, because we choose $x$ randomly from the set of all mesonic sites with total number $V_D$) yields
	\begin{equation}
		\frac{1}{3V_D}=2m.
	\end{equation}
	This means that the chosen probabilities do not meet the detailed balance condition. If we, however, allow this step to break detailed balance by instead following
	\begin{equation}
		6mV_DW_a(x)P_{x\hat{\mu}}=W'_a(x)P_{\hat{\mu}x}
	\end{equation}
	and then inspect a chain of worm updates starting at configuration $\mathcal{C}_0$ and ending at $\mathcal{C}_n$ with the closure of the worm, we see that this entire chain meets detailed balance:
	\begin{align}
		W(\mathcal{C}_0)P(\mathcal{C}_0\rightarrow\mathcal{C}_n)&=W(\mathcal{C}_0)P(\mathcal{C}_0\rightarrow\mathcal{C}_{1})\left(\prod_{k=1}^{n-2}P(\mathcal{C}_k\rightarrow\mathcal{C}_{k+1})\right)P(\mathcal{C}_{n-1}\rightarrow\mathcal{C}_n)\\
		&=\frac{1}{6mV_D}P(\mathcal{C}_1\rightarrow\mathcal{C}_{0})W(\mathcal{C}_1)\left(\prod_{k=1}^{n-2}P(\mathcal{C}_k\rightarrow\mathcal{C}_{k+1})\right)P(\mathcal{C}_{n-1}\rightarrow\mathcal{C}_n)\\
		&=\frac{1}{6mV_D}P(\mathcal{C}_1\rightarrow\mathcal{C}_{0})\left(\prod_{k=1}^{n-2}P(\mathcal{C}_{k+1}\rightarrow\mathcal{C}_{k})\right)W(\mathcal{C}_{n-1})P(\mathcal{C}_{n-1}\rightarrow\mathcal{C}_n)\\
		&=P(\mathcal{C}_1\rightarrow\mathcal{C}_{0})\left(\prod_{k=1}^{n-2}P(\mathcal{C}_{k+1}\rightarrow\mathcal{C}_{k})\right)P(\mathcal{C}_n\rightarrow\mathcal{C}_{n-1})W(\mathcal{C}_{n})\\
		&=W(\mathcal{C}_n)P(\mathcal{C}_n\rightarrow\mathcal{C}_0)
	.\end{align}
	So the update in its entirety satisfies detailed balance.
\end{itemize}

\subsubsection{Baryonic worm}
In a similar fashion we will now turn our attention to the baryonic worm updates and prove that they also obey detailed balance.
\begin{itemize}
	\item Step 2: Let $y$ be the site where the update takes place and $\hat{\mu}$ be the incoming direction of the worm. Let $\hat{\rho}$ be the outgoing direction. The detailed balance condition we have to fulfil is
	\begin{align}
		W_p(y)P_{\hat{\mu}\hat{\rho}}&=W'_p(y)P_{\hat{\rho}\hat{\mu}}\\
		\gamma^{\delta_{\mu0}(2k_{\hat{\mu}}+3|b_{\hat{\mu}}|)+\delta_{\rho 0}(2k_{\hat{\rho}}+3|b_{\hat{\rho}}|)} &\exp(3b_{\hat{\mu}}(\delta_{\hat{\mu},\hat{0}}-\delta_{\hat{\mu},-\hat{0}})\mu+3b_{\hat{\rho}}(\delta_{\hat{\rho},\hat{0}}-\delta_{\hat{\rho},-\hat{0}})\mu) P_{\hat{\mu}\hat{\rho}}=\\= \gamma^{\delta_{\mu0}(2k'_{\hat{\mu}}+3|b'_{\hat{\mu}}|)+\delta_{\rho 0}(2k'_{\hat{\rho}}+3|b'_{\hat{\rho}}|)} &\exp(3b'_{\hat{\mu}}(\delta_{\hat{\mu},\hat{0}}-\delta_{\hat{\mu},-\hat{0}})\mu+3b'_{\hat{\rho}}(\delta_{\hat{\rho},\hat{0}}-\delta_{\hat{\rho},-\hat{0}})\mu)P_{\hat{\rho}\hat{\mu}}
	.\end{align}
	Just as in the mesonic worm, we have to consider different cases:
	\begin{enumerate}
		\item $\hat{\mu}\neq\pm\hat{0}\neq\hat{\rho}\Rightarrow W_p(y)=W'_p(y)$:
		\begin{align}
			P_{\hat{\mu}\hat{\rho}}&=P_{\hat{\rho}\hat{\mu}}\\
			W_{\hat{\mu}\hat{\rho}}&=W_{\hat{\rho}\hat{\mu}}\\
			1&=1
		.\end{align}
		\item $\hat{\mu}=\pm\hat{0}\neq\hat{\rho}$: The weights now depend on what $k_{\hat{\mu}}$ and $b_{\hat{\mu}}$ were in the beginning.
		\begin{enumerate}
			\item $b_{\hat{\mu}}=1, k_{\hat{\mu}}=0\Rightarrow b'_{\hat{\mu}}=0, k_{\hat{\mu}}=0$, so the condition becomes
			\begin{align}
				\gamma^{2k_{\hat{\mu}}+3|b_{\hat{\mu}}|} \exp(3b_{\hat{\mu}}(\delta_{\hat{\mu},\hat{0}}-\delta_{\hat{\mu},-\hat{0}})\mu)P_{\hat{\mu}\hat{\rho}}&=
				\gamma^{2k'_{\hat{\mu}}+3|b'_{\hat{\mu}}|} \exp(3b'_{\hat{\mu}}(\delta_{\hat{\mu},\hat{0}}-\delta_{\hat{\mu},-\hat{0}})\mu)P_{\hat{\rho}\hat{\mu}}\\
				\gamma^3\exp(3(\delta_{\hat{\mu},\hat{0}}-\delta_{\hat{\mu},-\hat{0}})\mu)P_{\hat{\mu}\hat{\rho}}&=P_{\hat{\rho}\hat{\mu}}\\
				\gamma^3\exp(3(\delta_{\hat{\mu},\hat{0}}-\delta_{\hat{\mu},-\hat{0}})\mu)P_{\hat{\mu}\hat{\rho}}&=P_{\hat{\rho}\hat{\mu}}\\
				\gamma^3\exp(3(\delta_{\hat{\mu},\hat{0}}-\delta_{\hat{\mu},-\hat{0}})\mu)\gamma^{3\delta_{\hat{\rho},\pm\hat{0}}}\exp(3(\delta_{\hat{\rho},\hat{0}}-\delta_{\hat{\rho},-\hat{0}})\mu)&=\gamma^{3\delta_{\hat{\mu},\pm\hat{0}}}\exp(3(\delta_{\hat{\mu},\hat{0}}-\delta_{\hat{\mu},-\hat{0}})\mu)\\
				\gamma^3 \exp(3(\delta_{\hat{\mu},\hat{0}}-\delta_{\hat{\mu},-\hat{0}})\mu)&=\gamma^{3\delta_{\hat{\mu},\pm\hat{0}}} \exp(3(\delta_{\hat{\mu},\hat{0}}-\delta_{\hat{\mu},-\hat{0}})\mu)\\
				\gamma^3 \exp(3(\delta_{\hat{\mu},\hat{0}}-\delta_{\hat{\mu},-\hat{0}})\mu)&=\gamma^3 \exp(3(\delta_{\hat{\mu},\hat{0}}-\delta_{\hat{\mu},-\hat{0}})\mu)
			.\end{align}
			\item $b_{\hat{\mu}}=0, k_{\hat{\mu}}=3\Rightarrow b'_{\hat{\mu}}=-1, k'_{\hat{\mu}}=0$ yielding
			\begin{align}
				\gamma^{2k_{\hat{\mu}}+3|b_{\hat{\mu}}|} \exp(3b_{\hat{\mu}}(\delta_{\hat{\mu},\hat{0}}-\delta_{\hat{\mu},-\hat{0}})\mu)P_{\hat{\mu}\hat{\rho}}&=
				\gamma^{2k'_{\hat{\mu}}+3|b'_{\hat{\mu}}|} \exp(3b'_{\hat{\mu}}(\delta_{\hat{\mu},\hat{0}}-\delta_{\hat{\mu},-\hat{0}})\mu)P_{\hat{\rho}\hat{\mu}}\\
				\gamma^6 P_{\hat{\mu}\hat{\rho}}&=\gamma^3 \exp(-3(\delta_{\hat{\mu},\hat{0}}-\delta_{\hat{\mu},-\hat{0}})\mu)P_{\hat{\rho}\hat{\mu}}\\
				\gamma^6 \gamma^{3\delta_{\hat{\rho},\pm\hat{0}}}\exp(3(\delta_{\hat{\rho},\hat{0}}-\delta_{\hat{\rho},-\hat{0}})\mu)&=\gamma^3 \exp(-3(\delta_{\hat{\mu},\hat{0}}-\delta_{\hat{\mu},-\hat{0}})\mu)\gamma^{3\delta_{\hat{\mu},\pm\hat{0}}}\exp(3(\delta_{\hat{\mu},\hat{0}}-\delta_{\hat{\mu},-\hat{0}})\mu)\\
				\gamma^6&=\gamma^6
			.\end{align}
		\end{enumerate}
		\item $\hat{\mu}\neq\pm\hat{0}=\hat{\rho}$: The weights depend on the initial values of $k_{\hat{\rho}}$ and $b_{\hat{\rho}}$
		\begin{enumerate}
			\item $b_{\hat{\rho}}=0, k_{\hat{\rho}}=0\Rightarrow b'_{\hat{\rho}}=1,k_{\hat{\rho}}=0$
			\begin{align}
				\gamma^{2k_{\hat{\rho}}+3|b_{\hat{\rho}}|} \exp(3b_{\hat{\rho}}(\delta_{\hat{\rho},\hat{0}}-\delta_{\hat{\rho},-\hat{0}})\mu)P_{\hat{\mu}\hat{\rho}}&=
				\gamma^{2k'_{\hat{\rho}}+3|b'_{\hat{\rho}}|} \exp(3b'_{\hat{\rho}}(\delta_{\hat{\rho},\hat{0}}-\delta_{\hat{\rho},-\hat{0}})\mu)P_{\hat{\rho}\hat{\mu}}\\
				P_{\hat{\mu}\hat{\rho}}&=\gamma^3 \exp(3(\delta_{\hat{\rho},\hat{0}}-\delta_{\hat{\rho},-\hat{0}})\mu)P_{\hat{\rho}\hat{\mu}}\\							\gamma^3 \exp(3(\delta_{\hat{\rho},\hat{0}}-\delta_{\hat{\rho},-\hat{0}})\mu)&=\gamma^3 \exp(3(\delta_{\hat{\rho},\hat{0}}-\delta_{\hat{\rho},-\hat{0}})\mu)
			.\end{align}
			\item $b_{\hat{\rho}}=-1, k_{\hat{\rho}}=0\Rightarrow b'_{\hat{\rho}}=0,k_{\hat{\rho}}=3$
			\begin{align}
				\gamma^{2k_{\hat{\rho}}+3|b_{\hat{\rho}}|} \exp(3b_{\hat{\rho}}(\delta_{\hat{\rho},\hat{0}}-\delta_{\hat{\rho},-\hat{0}})\mu)P_{\hat{\mu}\hat{\rho}}&=
				\gamma^{2k'_{\hat{\rho}}+3|b'_{\hat{\rho}}|} \exp(3b'_{\hat{\rho}}(\delta_{\hat{\rho},\hat{0}}-\delta_{\hat{\rho},-\hat{0}})\mu)P_{\hat{\mu}\hat{\mu}}\\
				\gamma^3 \exp(-3(\delta_{\hat{\rho},\hat{0}}-\delta_{\hat{\rho},-\hat{0}})\mu)P_{\hat{\mu}\hat{\rho}}&=\gamma^6 P_{\hat{\rho}\hat{\mu}}\\
				\gamma^6 \exp(-3(\delta_{\hat{\rho},\hat{0}}-\delta_{\hat{\rho},-\hat{0}})\mu)\exp(3(\delta_{\hat{\rho},\hat{0}}-\delta_{\hat{\rho},-\hat{0}})\mu)&=\gamma^6\\
				\gamma^6&=\gamma^6
			.\end{align}
		\end{enumerate}
		\item $\hat{\mu}=\pm\hat{0}, \hat{\rho}=\pm\hat{0}$ Once again, the weights depend on the initial values of $k_{\hat{\mu}}$ and $b_{\hat{\mu}}$ but also on $k_{\hat{\rho}}$ and $b_{\hat{\rho}}$. The factors of the expression, however, behave in just the same way as for when one of them is $\neq\hat{0}$ so this calculation is omitted.
	\end{enumerate}
	The probabilities are, again, normalized by dividing by $W_D(y)=\sum_{\hat{\nu}}W_{\hat{\mu}\hat{\nu}}$, while only allowing steps to sites with a triple dimer or a baryonic loop.
	\item Step 3: Let $x$ be the active site of the update and $\hat{\mu}$ be the incoming direction of the worm. We differentiate between two cases:
	\begin{enumerate}
		\item $x$ is touched by a triple dimer in direction $\hat{\rho}$. We then change\\ $b_{\hat{\mu}}\rightarrow b_{\hat{\mu}}-1=b'_{\hat{\mu}},b_{\hat{\rho}}\rightarrow 1=b'_{\hat{\rho}},k_{\hat{\rho}}\rightarrow0=k'_{\hat{\rho}}$. The inverse step is to traverse the site along a baryonic loop updating $b_{\hat{\mu}}=1\rightarrow0=b'_{\hat{\mu}}, k_{\hat{\mu}}=0\rightarrow 3=k'_{\hat{\mu}},b_{\hat{\rho}}=-1\rightarrow0=b'_{\hat{\rho}}$ This leads to
		\begin{align}
			\left(\frac{(3-k_{\hat{\mu}})!(3-k_{\hat{\rho}})!}{3!k_{\hat{\mu}}!3!k_{\hat{\rho}}!}\right)P_{\hat{\mu}\hat{\rho}}&=			\left(\frac{(3-k'_{\hat{\mu}})!(3-k'_{\hat{\rho}})!}{3!k'_{\hat{\mu}}!3!k'_{\hat{\rho}}!(3!)^2}\right)P_{\hat{\rho}\hat{\mu}}\\
			\frac{1}{3!}P_{\hat{\mu}\hat{\rho}}&= \frac{3!}{(3!)^2}P_{\hat{\rho}\hat{\mu}}\\
			P_{\hat{\mu}\hat{\rho}}&= P_{\hat{\rho}\hat{\mu}}\\
			1&=1.
		\end{align} 
		\item $x$ is traversed by a baryon loop, s.t. there is a direction $\rho$ with $b_{\hat{\rho}}=-1$, but $b_{\hat{\mu}}=0$. We update\\
		$b_{\hat{\mu}}\rightarrow b_{\hat{\mu}}-1,b_{\hat{\rho}}=0$. Since no mesonic link variables are changed and the total number of baryonic links stays $2$, the weight is also unaffected, and because both steps are executed with probability $1$ this step fulfils detailed balance.
	\end{enumerate}
	\item Step 1 and Step 3 closing the worm: let $x$ be the site of the step and $\hat{\mu}$ be the outgoing (incoming) direction of step 1 (step 3). There are two possible states for site $x$ in step 1
	\begin{enumerate}
		\item $x$ is connected to a triple dimer in direction $\hat{\mu}$. We get for detailed balance:
		\begin{align}
			\left(\frac{(3-k_{\hat{\mu}})!}{3!k_{\hat{\mu}}!}\right)P_{x\hat{\mu}}&=\left(\frac{(3-k'_{\hat{\mu}})!}{3!k'_{\hat{\mu}}!(3!)^2}\right)P_{\hat{\mu}x}\\
			\frac{1}{3!}P_{x\hat{\mu}}&=\frac{3!}{(3!)^2}P_{\hat{\mu}x}\\
			1&=1
		.\end{align}
		\item $x$ is part of a baryon loop with $b_{\hat{\mu}}=-1$. We see that, once again, no mesonic link variables are updated and the site stays member of a baryon loop, so this step satisfies detailed balance.
	\end{enumerate}
	This time we do not run into the same problem as with the mesonic worm, where the beginning and ending of the worm do not fulfil detailed balance. However, it is important to note that the intermediate states of the lattices still do not contribute to the partition function, since they do not fulfil the constraints for the baryonic link variables $\sum_{\hat{\nu}}b_{\hat{\nu}}=0$.
\end{itemize}


\subsection{Observables}
\label{observables}
Here $\langle A\rangle$ means the average over the configurations generated by the worm algorithm.
\begin{itemize}
	\item the baryon density $\rho_B$ is given by
	\begin{align}
		\rho_B&=\frac{1}{V_SN_t}\frac{1}{Z}\frac{\partial}{\partial\mu}Z\\
		&=\frac{1}{V_sN_t}\frac{1}{Z}\frac{\partial}{\partial\mu}\left(\sum_{\{k,n,l\}}\prod_{b=(x,\hat{\mu})}\frac{(3-k_b)!}{3!k_b!}\gamma^{2k_b\delta_{\nu,0}}\prod_x\frac{3!}{n_x!}(2m_q)^{n_x}\prod_l\frac{1}{\prod_{x\in l}3!}\sigma(l)\gamma^{3N_{\hat{0}}}e^{3N_tr_l\mu}\right)\text{see \autoref{partition function equation}}\\
		&=\frac{1}{V_sN_t}\frac{1}{Z}\left(\sum_{\{k,n,l\}}\prod_{b=(x,\hat{\mu})}\frac{(3-k_b)!}{3!k_b!}\gamma^{2k_b\delta_{\nu,0}}\prod_x\frac{3!}{n_x!}(2m_q)^{n_x}\frac{\partial}{\partial\mu}\prod_l\frac{1}{\prod_{x\in l}3!}\sigma(l)\gamma^{3N_{\hat{0}}}e^{3N_tr_l\mu}\right)\\
		&=\frac{1}{V_sN_t}\frac{1}{Z}\left(\sum_{\{k,n,l\}}\prod_{b=(x,\hat{\mu})}\frac{(3-k_b)!}{3!k_b!}\gamma^{2k_b\delta_{\nu,0}}\prod_x\frac{3!}{n_x!}(2m_q)^{n_x}\left(\sum_l 3N_tr_l\right)\prod_l\frac{1}{\prod_{x\in l}3!}\sigma(l)\gamma^{3N_{\hat{0}}}e^{3N_tr_l\mu}\right)\\
		&=\frac{3}{V_S}\langle\sum_lr_l\rangle
	.\end{align}
	\item the energy density $\epsilon$ is given by
	\begin{align}
		\epsilon&=-\frac{1}{V_S}\frac{1}{Z}\frac{\partial}{\partial\beta}Z\\
		&=-\frac{1}{V_sN_t}\frac{1}{Z}\frac{\partial}{\partial a_t}Z\\
		&=-\frac{1}{V_sN_t}\frac{1}{Z}\frac{\partial\gamma}{\partial a_t}\frac{\partial}{\partial \gamma}Z\\
		&=-\frac{1}{V_sN_t}\frac{1}{Z}\frac{\partial\gamma}{\partial \frac{1}{f(\gamma)}}\frac{\partial\frac{1}{f(\gamma)}}{\partial a_t}\frac{\partial}{\partial \gamma}Z\\
		&=-\frac{1}{V_sN_t}\frac{1}{Z}\frac{\partial\gamma}{\partial \frac{1}{f(\gamma)}}\frac{\partial\frac{a_t}{a}}{\partial a_t}\frac{\partial}{\partial \gamma}Z\\
		&=-\frac{1}{V_sN_ta}\frac{1}{Z}\frac{\partial\gamma}{\partial \frac{1}{f(\gamma)}}\frac{\partial}{\partial \gamma}Z.
	\end{align}
	Here we run into a problem, since the function $h(\gamma)=\frac{1}{f(\gamma)}$ is not known. We, thus, elect to calculate $\tilde{\epsilon}=\frac{a}{\frac{\partial\gamma}{\partial h(\gamma)}}\epsilon$ instead, which we calculate as:\\
	\begin{alignat}{3}
		\tilde{\epsilon}&=-\frac{1}{V_sN_t}\frac{1}{Z}\frac{\partial}{\partial \gamma}&&Z\\
		&=-\frac{1}{V_sN_t}\frac{1}{Z}\frac{\partial}{\partial \gamma}&&\left(\sum_{\{k,n,l\}}\prod_{b=(x,\hat{\mu})}\frac{(3-k_b)!}{3!k_b!}\gamma^{2k_b\delta_{\nu,0}}\prod_x\frac{3!}{n_x!}(2m_q)^{n_x}\prod_l\frac{1}{\prod_{x\in l}3!}\sigma(l)\gamma^{3N_{\hat{0}}}e^{3N_tr_l\mu}\right)\\
		&=-\frac{1}{V_sN_t}\frac{1}{Z}&&\left(\left(\sum_{\{k,n,l\}}\left(\frac{\partial}{\partial \gamma}\prod_{b=(x,\hat{\mu})}\frac{(3-k_b)!}{3!k_b!}\gamma^{2k_b\delta_{\nu,0}}\right)\prod_x\frac{3!}{n_x!}(2m_q)^{n_x}\prod_l\frac{1}{\prod_{x\in l}3!}\sigma(l)\gamma^{3N_{\hat{0}}}e^{3N_tr_l\mu}\right)\right.\nonumber\\
		&&&+\left.\left(\sum_{\{k,n,l\}}\prod_{b=(x,\hat{\mu})}\frac{(3-k_b)!}{3!k_b!}\gamma^{2k_b\delta_{\nu,0}}\prod_x\frac{3!}{n_x!}(2m_q)^{n_x}\left(\frac{\partial}{\partial \gamma}\prod_l\frac{1}{\prod_{x\in l}3!}\sigma(l)\gamma^{3N_{\hat{0}}}e^{3N_tr_l\mu}\right)\right)\right)\\\nonumber\\
		&=-\frac{1}{V_sN_t}\frac{1}{Z}&&\left(\left(\sum_{\{k,n,l\}}\left(\sum_{b=(x,\hat{\mu})}2k_b\delta_{\nu,0}\right)\prod_{b=(x,\hat{\mu})}\frac{(3-k_b)!}{3!k_b!}\gamma^{2k_b\delta_{\nu,0}-1}\prod_x\frac{3!}{n_x!}(2m_q)^{n_x}\prod_l\frac{1}{\prod_{x\in l}3!}\sigma(l)\gamma^{3N_{\hat{0}}}e^{3N_tr_l\mu}\right)\right.\nonumber\\
		&&&+\left.\left(\sum_{\{k,n,l\}}\prod_{b=(x,\hat{\mu})}\frac{(3-k_b)!}{3!k_b!}\gamma^{2k_b\delta_{\nu,0}}\prod_x\frac{3!}{n_x!}(2m_q)^{n_x}\left(\sum_l 3N_{\hat{0}}\right)\prod_l\frac{1}{\prod_{x\in l}3!}\sigma(l)\gamma^{3N_{\hat{0}}-1}e^{3N_tr_l\mu}\right)\right)\\\nonumber\\
		&=-\frac{1}{V_SN_t}\frac{1}{\gamma}&&\langle2N_{Dt}\rangle+\langle3N_{Bt}\rangle\\
		&=-\frac{1}{V_SN_t}\frac{1}{\gamma}&&\langle2N_{Dt}+3N_{Bt}\rangle,
	\end{alignat}
	with $N_{Dt}=\sum_x k_{\hat{0}}(x)$ and $N_{Bt}=\sum_x |b_{\hat{0}}(x)|$ are the total number of dimer and (anti)baryonic links in $\hat{0}$-direction.\cite{fromm} The last step is possible, since $N_{Dt}$ and $N_{Bt}$ are independent.
\end{itemize}

\subsection{Representation of the grid in memory}
Computer memory is best described as a long tape of memory cells, meaning we have to map our $d$-dimensional grid onto a one-dimensional part of this tape. A simple way to do this linearization is just by enumerating each site row by row. This means every site gets indexed by
\begin{equation}
	\text{index}=\sum_{i=0}^d \left(x_i \underbrace{\prod_{j=0}^iN_j}_{\substack{\text{so called}\\\text{strides}}}\right),
\end{equation}
where $N_j$ is the extent of the lattice in direction $j$. A lattice with $d=3$, $N_0=N_1=10$ and $N_2=3$ would be indexed as depicted in \autoref{memory layout}. The values for $k$ and $b$ are now stored in a linearized array of shape $N_0\times\dots\times N_d\times d\times 2$, where the linearization of the last two dimensions is left to the compiler in this case. In order to calculate the index of a neighbouring site, we first have to compute the coordinates from the index. In practice it is enough to calculate the coordinate $x_\mu$, if we move in direction $\pm\hat{\mu}$, since we only need to check whether we are at the boundary of the lattice. The coordinates can be calculated recursively by
\begin{align}
	x_{-1}&=0\\
	i_{-1}&=\text{index}\\
	x_{n+1}&=i_n \mod N_n\\
	i_{n+1}&= \left\lfloor\frac{i_n-x_{n+1}}{N_n}\right\rfloor.
\end{align}
Now we can determine, whether a move in direction $\hat{\mu}$ would loop around the lattice, which is equivalent to moving $N_{\mu}-1$ sites in direction $-\hat{\mu}$ or just one site in direction $\hat{\mu}$. To calculate the new index, we use
\begin{equation}
	\text{index}'=\text{index}+S_\mu n,
\end{equation}
with $S_\mu = \prod_{j=0}^\mu N_j$ the stride in direction $\mu$ and $n$ the number of steps to take in direction $\hat{\mu}$. It may be tempting to pre-compute the indices of the neighbouring cells or the coordinates of each site and also save them in an array. However it turns out that pre-computing the strides and re-computing the neighbour's index is faster for bigger lattices, since the values for $k$ and $b$ can remain in the cache of the processor. This outweighs the disadvantage of having to perform multiple division operations to find the coordinates.
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{images/memory/out/layout.pdf}
	\caption{memory layout}
	\label{memory layout}
\end{figure}

\subsection{Error estimation}\label{error estimation}
To estimate the error for the datapoints, there are several different approaches. It is possible to calculate statistical deviations for the primary quantities $\langle O\sigma\rangle$ and $\langle\sigma\rangle$ then propagate this error to the secondary quantities $\frac{\langle O\sigma\rangle}{\langle \sigma\rangle}$ (e.g. \autoref{error propagation}). This implementation, however, works with the so called \textins{jackknife variance estimator}, which allows us to measure the errors for secondary quantities immediately. When incorporating the autocorrelation, it gives a good estimate of the standard deviation \cite{bloch}. This method works by generating \textit{resamples} on the measured data and then computing the variance of the resamples. Supposing we have a sample of measurements $S=\{\mu_t: t\in\{1,\dots,N\}\}$ of some quantity. It is essential, that these measurements are independent and if they are not, we must reduce the sample to independent measurements by only using every $2\tau_{\text{int}}$th one, where $\tau_{\text{int}}$ is the autocorrelation time. Now construct $N$ jackknife resamples of $N-1$ measurements, by removing each of them from the original sample: $S_i=\{\mu_1,\dots,\mu_{i-1},\mu_{i+1},\dots,\mu_N\}$, for $i\in\{1,\dots,N\}$. From those resamples, we now compute $N$ measurements for a secondary quantity $Q$: $Q_i=Q(S_i)$. The jackknife estimate for the standard error is then given by
\begin{equation}
	\sigma_Q=\sqrt{\sum_{i=1}^{N}(Q_i-\bar{Q}_\text{Jack})^2},
\end{equation}
with $\bar{Q}_\text{Jack}=\frac1N\sum_{i=1}^NQ_i$\cite{bloch}.