% !TeX spellcheck = en_GB
\section{Partition function}
\subsection{Deriving the partition function for the lattice}
The partition function in QCD is given by
\begin{equation} 
  Z(\mu, T, m_q) = \int \mathcal{D}\bar{\Psi}\mathcal{D}\Psi\mathcal{D}U e^{-S_{\text{YM}}-S_\text{F}}
  \label{partition function},
\end{equation}
where $U$ is the gauge field, $\bar{\Psi}$ and $\Psi$ are the fermion fields, $S_\text{YM}$ is the gauge action and $S_F$ is the fermion action. Since we will be working in the strong coupling limit, we can ignore the $S_\text{YM}$ part and focus on the fermion contribution\cite{fromm}.\\
In the continuous theory the fermion action is given by
\begin{equation}
  S_\text{F}=\int \text{d}^4x\bar{\psi}(x)(\slashed{\partial} + m)\psi(x).
\end{equation}
Since we will be working on a lattice, the derivative becomes a finite difference and we arrive at (setting $a=1$)
\begin{equation}
  S_\text{F, lat} = \sum_x\left(\sum_{\mu = 0}^3 \bar{\psi}_x\gamma_\mu\frac{\psi_{x+\hat{\mu}} - \psi_{x-\hat{\mu}}}{2} + m_q\bar{\psi}_x\psi_x\right).
\end{equation}
This action however suffers from the so called fermion doubler problem, where particles at the borders of the Brillouin zone behave as if they had small momenta and thus interfere with the calculation of observables\cite{lqcd}.  In order to mitigate their effects, numerous methods have been developed, each with their own advantages and disadvantages, since the Nielsen-Niomiya theorem \cite{lqcd, nielsen} disallows avoiding fermion doublers while preserving chiral symmetry.
Here, staggered fermions will be used.\\
First we perform a change of variables
\begin{equation}
  \psi(x) = T(x)\hat{\chi}(x),\qquad \bar{\psi}=\hat{\bar{\chi}}(x)T^\dagger(x),
\end{equation}
where the $T$s are unitary matrices, s.t.
\begin{equation}
  T^\dagger(x)\gamma_\mu T(x+\hat{\mu})=\eta_{\hat{\mu}} (x) \in \mathbb{C}\cdot\mathds{1}_4,
\end{equation}
with the Euclidean gamma matrices $\gamma_\mu$.\\
Note that for $\mu\neq\nu$
\begin{align}
  \eta_{\hat{\mu}}(x)\eta_\nu(x+\hat{\mu})\eta_{\hat{\mu}}^\dagger(x+\hat{\nu})\eta_\nu^\dagger(x) &= T^\dagger(x)\gamma_\mu T(x+\hat{\mu}) T^\dagger(x+\hat{\nu})\gamma_\nu T(x+\hat{\mu}+\hat{\nu})T^\dagger(x+\hat{\mu} + \hat{\nu})\gamma_\mu^\dagger T(x+\hat{\nu})T^\dagger(x+\hat{\nu})\gamma_\nu^\dagger T(x)\\
  &=T^\dagger(x)\gamma_\mu\gamma_\nu\gamma_\mu^\dagger\gamma_\nu^\dagger T(x)\\
  &=T^\dagger(x)\gamma_\mu\gamma_\nu\gamma_\mu\gamma_\nu T(x)\\
  &=T^\dagger(x)\left(-\gamma_\nu\gamma_\mu\gamma_\mu\gamma_\nu + \{\gamma_\mu,\gamma_\nu\}\gamma_\mu\gamma_\nu\right) T(x)W\\
  &=T^\dagger(x)(-1+2\underbrace{\delta_{\mu\nu}}_{0 \text{, since, $\mu\neq\nu$}}\gamma_\mu\gamma_\nu)T(x)\\
  &=-T^\dagger(x)T(x) = -1
.\end{align}
This puts a constraint on the $T$s, for which a possible choice is $T(x)=\gamma_0^{x_0}\gamma_1^{x_1}\gamma_2^{x_2}\gamma_3^{x_3}$. From this we get
\begin{align}
  \eta_{\hat{\mu}}(x)&=(\gamma_0^{x_0}\gamma_1^{x_1}\gamma_2^{x_2}\gamma_3^{x_3})^\dagger\gamma_\mu\gamma_0^{x_0\pm\delta_{\mu,0}}\gamma_1^{x_1\pm\delta_{\mu,1}}\gamma_2^{x_2\pm\delta_{\mu,2}}\gamma_3^{x_3\pm\delta_{\mu,3}}\\
  &={\gamma_3^\dagger}^{x_3}{\gamma_2^\dagger}^{x_2}{\gamma_1^\dagger}^{x_1}{\gamma_0^\dagger}^{x_0}\gamma_\mu\gamma_0^{x_0+\delta_{\mu,0}}\gamma_1^{x_1+\delta_{\mu,1}}\gamma_2^{x_2+\delta_{\mu,2}}\gamma_3^{x_3+\delta_{\mu,3}}\\
  &=\gamma_3^{x_3}\gamma_2^{x_2}\gamma_1^{x_1}\gamma_0^{x_0}\gamma_\mu\gamma_0^{x_0\pm\delta_{\mu,0}}\gamma_1^{x_1\pm\delta_{\mu,1}}\gamma_2^{x_2\pm\delta_{\mu,2}}\gamma_3^{x_3\pm\delta_{\mu,3}}\\
  \substack{\text{if $\hat{\mu}$ is in positive direction,}\\\text{ otherwise skip to next line}} &=\gamma_3^{x_3}\gamma_2^{x_2}\gamma_1^{x_1}\gamma_0^{x_0}\gamma_0^{x_0+2\delta_{\mu,0}}\gamma_1^{x_1+2\delta_{\mu,1}}\gamma_2^{x_2+2\delta_{\mu,2}}\gamma_3^{x_3+2\delta_{\mu,3}}(-1)^{\sum_{\nu<\mu}x_\nu}\\
  &=\gamma_3^{x_3}\gamma_2^{x_2}\gamma_1^{x_1}\gamma_0^{x_0}\gamma_0^{x_0}\gamma_1^{x_1}\gamma_2^{x_2}\gamma_3^{x_3}(-1)^{\sum_{\nu<\mu}x_\nu}\\
  &=(-1)^{\sum_{\nu<\mu}x_\nu}
.\end{align}
Note that $\eta_{\hat{\mu}}(x) = \eta_{\hat{\mu}}(x+\hat{\mu})$ and $\eta^\dagger = \eta$, since $\eta=\pm \mathds{1}_4$, so $T^\dagger(x)\gamma_\mu T(x+\hat{\mu})=T^\dagger(x)\gamma_\mu T(x-\hat{\mu})=\eta_{\hat{\mu}}(x)$\\
Inserting this into the action gives
\begin{align}
  S_\text{F, stag}&=\sum_x\left(\sum_{\mu=0}^3\hat{\bar{\chi}}(x)T^\dagger(x)\gamma_\mu \frac{T(x+\hat{\mu})\hat{\chi}_{x+\hat{\mu}}-T(x-\hat\mu)\hat{\chi}_{x-\hat{\mu}}}{2} +m_q\hat{\bar{\chi}}_x\hat{\chi}_x \right)\\
  &=\sum_x\left(\sum_{\mu=0}^3\eta_{\hat{\mu}}(x)\hat{\bar{\chi}}(x) \frac{\hat{\chi}_{x+\hat{\mu}}-\hat{\chi}_{x-\hat{\mu}}}{2} +m_q\hat{\bar{\chi}}_x\hat{\chi}_x \right)
.\end{align}
In order to make this expression gauge invariant on the lattice, we have to replace the finite difference by a covariant derivative yielding
\begin{equation}
  S_\text{F, stag}=\frac12\sum_x\sum_{\mu=0}^3\eta_{\hat{\mu}}(x)\left(\hat{\bar{\chi}}_xU_{\hat{\mu}}(x)\hat{\chi}_{x+\hat{\mu}}-\hat{\bar{\chi}}_{x+\hat{\mu}}U^\dagger_{\hat{\mu}}(x)\hat{\chi}_x+2m_q\hat{\bar{\chi}}_x\hat{\chi}_x\right).
\end{equation}
For convenience we define $\chi= \sqrt{2}\hat{\chi}$:
\begin{equation}
	S_\text{F, stag}=\sum_x\sum_{\mu=0}^3\eta_{\hat{\mu}}(x)\left(\bar{\chi}_xU_{\hat{\mu}}(x)\chi_{x+\hat{\mu}}-\bar{\chi}_{x+\hat{\mu}}U^\dagger_{\hat{\mu}}(x)\chi_x+2m_q\bar{\chi}_x\chi_x\right).
\end{equation}
The partition function then reads
\begin{equation}
	Z=\int\mathcal{D}U\mathcal{D}\chi\mathcal{D}\bar{\chi}e^{-S_\text{F, stag}}.
\end{equation}
The subscript \textit{stag} will be omitted from now on.
\subsection{Introducing temperature and chemical potential}
\subsubsection{Temperature}
The fact that in classical thermodynamics the partition function is given by
\begin{equation}
	Z(T)=\text{Tr}e^{-\hat{H}/T}
\end{equation}
allows us to rewrite the trace in a field configuration basis (also defining $\beta=\frac{1}{T}$):
\begin{align}
	\text{for fermion fields}\qquad Z(\beta)&=\int\mathcal{D}\phi(\tau=0) \bra{-\phi}e^{-\beta\hat{H}}\ket{\phi}\\
	\text{for gauge fields}\qquad Z(\beta)&=\int\mathcal{D}\phi(\tau=0) \bra{\phi}e^{-\beta\hat{H}}\ket{\phi},
\end{align}
where $\mathcal{D}\phi(\tau=0)$ means, that we only integrate over the spacial configurations at imaginary time $\tau=0$\cite[p.19ff., 87ff.]{thermalFT}. Note that the minus sign in the bra comes from the definition of the trace of an operator for fermion fields. We can now interpret $e^{-\beta\hat{H}}$ as a time evolution in imaginary time and thus are integrating over a propagator $K(\pm\phi, -i\beta, \phi, 0)$. Inserting the path integral expression for this propagator, we arrive at
\begin{align}
	Z(\beta)&=\int\mathcal{D}\phi(\tau) \exp\left(-\int_0^\beta\text{d}\tau\mathcal{L}\right)\\
	&=\int\mathcal{D}\phi(\tau) \exp\left(-S(\beta)\right),
\end{align}
where we only integrate over paths that satisfy the boundary conditions $\pm\phi(\beta)=\phi(0)$. This immediately tells us that
\begin{enumerate}
	\item The lattice extent in time direction is connected to the temperature by $N_t a_t = \beta = \frac{1}{T}$
	\item we need to implement anti-periodic boundary conditions for our fermion fields in time direction \\$\chi(\vec{x},t+N_t)=-\chi(\vec{x},t)$
	\item we need to implement periodic boundary conditions for our gauge fields in time direction \\$U_{\hat{\mu}}(\vec{x},t+N_t) = U_{\hat{\mu}}(\vec{x},t)$
\end{enumerate}
To vary the temperature one could change the number of lattice sites in time direction, however especially for high temperatures this is very coarse. A better approach is to introduce an anisotropy between time and space lattice spacing by multiplying the temporal part of the action with some factor $\gamma$. The action then reads:
\begin{align}
	S_\text{F}=\frac12&\sum_x\left[\left(\sum_{\mu=1}^3\eta_{\hat{\mu}}(x)\left(\hat{\bar{\chi}}_xU_{\hat{\mu}}(x)\hat{\chi}_{x+\hat{\mu}}-\hat{\bar{\chi}}_{x+\hat{\mu}}U^\dagger_{\hat{\mu}}(x)\hat{\chi}_x+2m_q\hat{\bar{\chi}}_x\hat{\chi}_x\right)\right)\right.\nonumber\\
	&+\left.\gamma\left(\hat{\bar{\chi}}_xU_{\hat{0}}(x)\hat{\chi}_{x+\hat{0}}-\hat{\bar{\chi}}_{x+\hat{0}}U^\dagger_{\hat{0}}(x)\hat{\chi}_x+2m_q\hat{\bar{\chi}}_x\hat{\chi}_x\right)\right].
\end{align}
Note that $\eta_{\hat{0}}(x)=1$ always.\\
It is also important to note, that this anisotropy $\gamma$ in the action does not directly translate to the anisotropy in the lattice spacing $f(\gamma)=\frac{a}{a_t}$. Only in the weak coupling limit $f(\gamma)=\gamma$ holds\cite{fromm}.
\subsubsection{Chemical potential}
We begin by examining the continuous Lagrangian
\begin{equation}
	\mathcal{L}=\frac12 \text{tr}(F_{\mu\nu}F_{\mu\nu})+\bar{\psi}(\gamma_\mu[\partial_\mu+igA_\mu]+m)\psi,
\end{equation}
with gauge field strength tensor $F_{\mu\nu}$ and gluon field $A_\mu$ \cite{fromm}.\\
We can easily see, that this Lagrangian has a so called vector symmetry $U_V(1)$:
\begin{align}
	\bar{\psi}&\rightarrow\bar{\psi}e^{-i\theta_V}\\
	\psi&\rightarrow e^{i\theta_V}\psi.
\end{align}
Noether's theorem \cite{qed} yields a conserved 4-current $\tilde{j}$:
\begin{align}
	\theta_V\tilde{j}_\mu&=\underbrace{\frac{\partial\mathcal{L}}{\partial(\partial_\mu\bar{\psi})}}_{0}\delta\bar{\psi}+\frac{\partial\mathcal{L}}{\partial(\partial_\mu\psi)}\delta\psi\\
	&=\bar{\psi}\gamma_\mu(i\theta_V\psi)
.\end{align}
Obviously $j=-i\tilde{j}=\bar{\psi}\gamma_\mu\psi$ is also conserved.
The conserved charge $n:=J_0:=\int_V\text{d}^3xj_0=\int_V\text{d}^3x\bar{\psi}\gamma_0\psi$, corresponds to quark number. In order to influence this quantity, the chemical potential should couple to the quark number by a term $\mu\bar{\psi}\gamma_0\psi$ in the Lagrangian. This can be achieved by letting $\partial_0\rightarrow\partial_0+\mu$\cite{fromm}.\\
On the lattice the gauge links incorporate the derivative, so they have to be altered by letting \\
$U_{\hat{0}}(x)\rightarrow e^{\mu}U_{\hat{0}}(x), U^\dagger_{\hat{0}}(x)\rightarrow e^{-\mu}U^\dagger_{\hat{0}}(x)$\cite{fromm}.\\
Putting everything together our action now reads
\begin{align}
	S_\text{F}&=\frac12\sum_x\left[\left(\sum_{\mu=1}^3\eta_{\hat{\mu}}(x)\left(\hat{\bar{\chi}}_xU_{\hat{\mu}}(x)\hat{\chi}_{x+\hat{\mu}}-\hat{\bar{\chi}}_{x+\hat{\mu}}U^\dagger_{\hat{\mu}}(x)\hat{\chi}_x+2m_q\hat{\bar{\chi}}_x\hat{\chi}_x\right)\right)\right.\\&´\left.+\gamma\left(\hat{\bar{\chi}}_xe^{\mu}U_{\hat{0}}(x)\hat{\chi}_{x+\hat{0}}-\hat{\bar{\chi}}_{x+\hat{0}}e^{-\mu}U^\dagger_{\hat{0}}(x)\hat{\chi}_x+2m_q\hat{\bar{\chi}}_x\hat{\chi}_x\right)\right]
.\end{align}
\subsection{Worm formulation}\label{worm formulation}
Because we neglect the gauge action, the integral over the gauge links in the partition function factorizes
\begin{align}
	Z&=\int\mathcal{D}U\mathcal{D}\chi\mathcal{D}\bar{\chi}e^{-S_\text{F, stag}}\\
	&=\int\prod_x\left(\dif\chi_x\dif\bar{\chi}_xe^{2m_q\bar{\chi}_x\chi_x}\prod_{\mu=1}^3
	\left[\dif U_{\hat{\mu}}(x)e^{\eta_{\hat{\mu}}(x)\left(\bar{\chi}_xU_{\hat{\mu}}(x)\chi_{x+\hat{\mu}}-\bar{\chi}_{x+\hat{\mu}}U_{\hat{\mu}}^\dagger(x)\chi_x\right)}\right]\right.\nonumber\\
	&\times\left.\dif U_{\hat{0}}(x)e^{\gamma\left(e^{\mu}\bar{\chi}_xU_{\hat{0}}(x)\chi_{x+\hat{0}}-e^{-\mu}\bar{\chi}_{x+\hat{0}}U_{\hat{0}}^\dagger(x)\chi_x\right)}\right)\\
	&=\int\prod_x\left(\dif\chi_x\dif\bar{\chi}_xe^{2m_q\bar{\chi}_x\chi_x}\prod_{\mu}z(x,\mu)\right)
\end{align}
with \textit{one-link integral} $z(x,\mu)$, that can be rewritten as (see \autoref{one_link} for details)
\begin{align}
	z(x,\nu)&=\int\dif U_{\hat{\nu}}(x)\exp\left[{\eta_{\hat{\nu}}(x)\gamma^{\delta_{\nu,0}}\left(e^{\delta_{\nu,0}\mu}\bar{\chi}_xU_{\hat{\nu}}(x)\chi_{x+\hat{\nu}}-e^{-\delta_{\nu,0}\mu}\bar{\chi}_{x+\hat{\nu}}U_{\hat{\nu}}^\dagger(x)\chi_x\right)}\right]\\
	&=\sum_{k_{x,\hat{\nu}}=0}^3\left\{\frac{(3-k_{x,\hat{\nu}})!}{3!k_{x,\hat{\nu}}!}((\eta_{\hat{\mu}}(x)\gamma^{\delta_{\mu,0}})^2M(x)M(y))^{k_{x,\hat{\nu}}}\right\}+(\rho(x,y)^3\bar{B}(x)B(y)-\rho(y,x)^3\bar{B}(y)B(x)),
\end{align}
with
\begin{align}
	\rho(x,y)&=\eta_{(y-x)}(x)\cdot\left\{
	\begin{array}{ll}
		\gamma\exp(\pm\mu) & (y-x)_0=\pm1 \\
		1 & \text{otherwise}\\
	\end{array}
	\right.\\
	M(z)&=\bar{\chi}_z\chi_z\\
	B(z)&=\frac{1}{3!}\epsilon_{i_1i_2i_3}\chi_{z,i_1}\chi_{z,i_2}\chi_{z,i_3},
\end{align}
where $i_k$ are color indices\cite{fromm}.\\
When we multiply out the product over sites and directions, we see that every summand must include all Grassmanns for every site exactly once to contribute to the partition function. This can be achieved in two separate ways for each site $x$:
\begin{enumerate}
	\item if the term contains the term $\bar{B}(y)B(x)$ for some neighbour site $y$. This necessitates the term also containing $\bar{B}(x)B(z)$ for another neighbour $z$. $\bar{B}(x)B(x)$ now employs all degrees of freedom for site $x$, so another term like $M(x)$ or $B(x)$ is not allowed. Also in the exponential corresponding to $x$ only the constant term survives. This means, that baryons must form oriented, self-avoiding loops in order to contribute to $Z$. It is useful to define a baryonic link variable $b_{\hat{\rho}}(x)\in\{-1,0,1\}$ so that terms with a term $\bar{B}(y)B(x)$ ($\bar{B}(x)B(z)$) correspond to a configuration with $b_{(x-y)}(x)=1$ ($b_{(z-x)}(x)=-1$). The constraint $\sum_{\hat{\rho}}b_{\hat{\rho}}(x)=0$ thus arises naturally. Note that the case $y = z$ is equivalent to $(M(x)M(y))^3$ and thus covered by the dimers.
	\item if the term contains the product $\prod_{\text{neighbours }y}(M(x)M(y))^{k_{(y-x)}(x)}$. Those can be understood as non-oriented links between $x$ and its neighbours, so called dimers. We call $k_{\hat{\nu}}(x)$ \textit{mesonic link-occupation number}. The exponential then \enquote{fills up} the term with $n_x=3-\sum_{\hat{\nu}}k_{\hat{\nu}}(x)$, so called monomers $M(x)^{n_x}$, s.t. the constraint $n_x+\sum_{\hat{\nu}}k_{\hat{\nu}}(x)=3$ must be satisfied in order to contribute to the partition function.
\end{enumerate}
The remaining integral in $Z$ can be solved by applying the relation (for derivation see \autoref{helper grassman})
\begin{equation}
\int \dif \chi_x \dif \bar{\chi}_x e^{2m_q\bar{\chi}_x\chi_x}(\bar{\chi}_x\chi_x)^k=\frac{3!}{n_x!}(2m_q)^{n_x},
\end{equation}
with $n_x = 3-k$ (since we will be working in the chiral limit, $n_x$ will actually always be 0).\\
After integration we arrive at the partition function, which is later used for the worm algorithm:
\begin{equation}\label{partition function equation}
	Z=\sum_{\{k,n,l\}}\prod_{b=(x,\hat{\mu})}\frac{(3-k_b)!}{3!k_b!}\gamma^{2k_b\delta_{\nu,0}}\prod_x\frac{3!}{n_x!}(2m_q)^{n_x}\prod_lw(l),
\end{equation}
where the first term comes from dimers, the second from monomers and the third term is the product over weights of the baryon loops, given by
\begin{equation}
	w(l)=\frac{1}{\prod_{x\in l}3!}\sigma(l)\gamma^{3N_{\hat{0}}}e^{3N_tr_la_t\mu},
\end{equation}
with \textit{sign} $\sigma(l)=(-1)^{r_l+N_-(l)+1}\prod_{b=(x,\hat{\mu})\in l}\eta_{\hat{\mu}}(x)$, $N_{\hat{0}}$ and $r_l$\label{sign} the number of links and windings in time direction respectively and finally $N_-$ the number of links in any negative direction\cite{fromm}.